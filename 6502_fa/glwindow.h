#ifndef GLWINDOW_H
#define GLWINDOW_H

#pragma once
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "glew32s.lib")
#define GLEW_STATIC 
#include "GLew/glew.h"
#include "GLew/wglew.h"
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

class GLWindow {
	WNDCLASSEX	wnd_class;
	HINSTANCE	instance;
	LPSTR		title;
	static char* classname;
	static bool	class_registered;
	int			height;
	int			width;
	bool		activated;
	bool		confined;

	static void do_nothing();
	void create_wnd_class();
	bool create_window(void (*init_function)(void));

public:
	HGLRC	render_context;
	HDC		device_context;
	HWND	handle;
	void	(*render_function)(void);
	void	(*keydown_function)(WPARAM );
	void	(*keyup_function)(WPARAM );
	void	(*mousemove_function)(WPARAM, LPARAM );
	void	(*leftdown_function)(WPARAM );
	void	(*leftup_function)(WPARAM );
	bool	(*proc_function)( UINT message, WPARAM wParam, LPARAM lParam );
	void	(*quit_function)();

	static const int default_height;
	static const int default_width;

	void init(void (*init_function)(void) = do_nothing);
	void show();
	void show(int);
	void update();
	void destroy();

	void confine_mouse();
	void release_mouse();
	POINT get_mouse();

	GLWindow(HINSTANCE instance, LPSTR title = classname, int width = default_width, int height = default_height);
	GLWindow();

	LRESULT CALLBACK procedure(UINT message, WPARAM wParam, LPARAM lParam);
	static LRESULT CALLBACK procedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static bool default_extra_proc(UINT message, WPARAM wParam, LPARAM lParam);
};

int SetDCPixelFormat(HDC hDC);
void CreateConsole(const unsigned int max_lines = 500);

#endif