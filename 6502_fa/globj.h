#ifndef GLOBJ_H
#define GLOBJ_H

#include <vector>
#include <string>

struct Point3 {
	float x;
	float y;
	float z;

	Point3();
	Point3(float x, float y, float z);
	Point3(std::string& s, size_t offset = 0);
};

//std::istream& operator>> (std::istream& stream, Point3& p);

struct FaceRef {
	int v;
	int vt;
	int vn;

	FaceRef();
	FaceRef(int v, int vt = 0, int vn = 0);
};
//std::istream& operator>> (std::istream& stream, FaceRef& f);

typedef std::vector<FaceRef> Face;

struct Object {
	std::vector<Point3> v, vt, vn;
	std::vector<Face> face;


	Object();
	Object(char* filename);
	void clear();
	void render();
	void render_normals();
private:
	int parse_file(char* filename);
	bool parse_face(Face& f, std::string& s);
};

struct Group {
	std::string name;
	int from;
	int to;
};

struct Material {
	float Ka[3], Kd[3], Ks[3];
	std::string tex_filename;
};

#endif