#pragma once
#include <cstdint>

namespace Nes {

	struct System;

	// Part of the CPU, but we split it up for neatness. Hooked to memory
	struct Apu {
		System& system;

		//Sequencer
		unsigned int frame_counter;
		unsigned int scanline_counter;
		unsigned int sample_counter;

		//Common channel registers
		uint8_t length_count[4];

		//Pulse registers/tables
		static const unsigned char duration_table[0x20];
		static const unsigned char duty_table[4];
		bool envelope_start_flag[2];
		bool sweep_reload_flag[2];

		uint8_t sweep_divider[2];
		uint8_t envelope[2];
		uint8_t envelope_divider[2];

		float pulse_phase[2];
		unsigned short pulse_out[2][800];

		//Triangle register/tables
		static const unsigned char triangle_table[32];
		bool triangle_halt_flag;
		bool linear_reload_flag;
		float triangle_phase;

		uint8_t linear_counter;

		unsigned short triangle_out[800];

		//Noise channel
		static const unsigned short noise_period_table[16];
		unsigned int lsfr_reg;
		bool noise_start_flag;
		float noise_fractional_sample;
		uint8_t noise_envelope;
		uint8_t noise_envelope_divider;
		float noise_out[800];

		//render target: 1 frame of 48000Hz, 16-bit mono @ 60Hz (TODO: make modifiable)
		unsigned short buffer[800 * 2];

		Apu(System& system);

		//pAPU $4000/4 Pulse 1,2 Control Register
		union {
			struct {
				uint8_t n : 4;					//----3210 decay time or volume
				uint8_t envelope_disable : 1;	//---4---- 0:envelope,1:constant volume
				uint8_t length_disable : 1;		//--5----- 0:use length counter,1:length counter off
				uint8_t duty : 2;				//76------ duty cycle 0:12.5%,1:25%,2:50%,3:75%
			};
			uint8_t byte;
		} pulse_ctrl[2];

		//pAPU $4001/5 Pulse 1,2 Ramp control
		union {
			struct {
				uint8_t shift : 3;				//-----210 shift count
				uint8_t direction : 1;			//----3--- 0:forward,1:backwards
				uint8_t period : 3;				//-654---- Add 1 to get period
				uint8_t enable : 1;				//7------- 0:off,1:on
			};
			uint8_t byte;
		} pulse_sweep[2];

		//pAPU $4002/6 Pulse 1,2 Fine tune
		uint8_t pulse_period_low[2];

		//pAPU $4003/7 Pulse 1,2 Coarse tune
		union {
			struct {
				uint8_t period_high : 3;		//-----210 period high
				uint8_t length_index : 5;		//76543--- length count
			};
			uint8_t byte;
		} pulse_coarse[2];

		//pAPU $4008 Triangle control
		union {
			struct {
				uint8_t reload : 7;
				uint8_t control_flag : 1;
			};
			uint8_t byte;
		} triangle_ctrl;

		//pAPU $400A Triangle Timer Low
		uint8_t triangle_period_low;

		//pAPU $400B Triangle Timer high
		union {
			struct {
				uint8_t period_high : 3;
				uint8_t length_index : 5;
			};
			uint8_t byte;
		} triangle_coarse;

		//pAPU $400C Noise control
		union {
			struct {
				uint8_t n : 4;
				uint8_t envelope_disable : 1;
				uint8_t length_disable : 1;
				uint8_t dummy : 2;
			};
			uint8_t byte;
		} noise_ctrl;

		//pAPU $400E Mode
		union {
			struct {
				uint8_t period_index : 4;
				uint8_t dummy : 3;
				uint8_t mode : 1;
			};
			uint8_t byte;
		} noise_period;

		//pAPU $400F Noise length
		union {
			struct {
				uint8_t dummy : 3;
				uint8_t length_index : 5;
			};
			uint8_t byte;
		} noise_coarse;

		//pAPU $4015 Sound register
		union {
			struct {
				uint8_t pulse1_enable : 1;		//-------0 0:off,1:on
				uint8_t pulse2_enable : 1;		//------1- 0:off,1:on
				uint8_t triangle_enable : 1;	//-----2-- 0:off,1:on
				uint8_t noise_enable : 1;		//----3--- 0:off,1:on
				uint8_t dmc_enable : 1;			//---4---- 0:off,1:on
				//-6------ (read only) Frame IRQ
				//7------- (read only) DMC IRQ
			};
			uint8_t byte;
		} master_control;

		//pAPU $4017 Frame control
		union {
			struct {
				uint8_t dummy : 6;				//--543210
				uint8_t disable_irq : 1;		//-6------ Disable IRQ=1
				uint8_t five_frame : 1;			//7------- 0:4 frame, 1: 5 frame sequence
			};
			uint8_t byte;
		} frame_control;

		void init();

		//Register control
		void master_control_write(uint8_t data);
		void pulse_coarse_write(int unit, uint8_t data);
		void triangle_coarse_write(uint8_t data);
		void pulse_sweep_write(int unit, uint8_t data);
		void noise_coarse_write(uint8_t data);
		void frame_control_write(uint8_t data);
		uint8_t status_read() const;

		//
		void pulse_generate(int unit, int samples);
		void triangle_generate(int samples);
		void noise_generate(int samples);

		float integrate_noise();
		void next_noise_bit();

		uint8_t get_envelope(int unit) const;
		uint8_t get_noise_envelope() const;
		float get_pulse_frequency(int unit) const;
		unsigned short get_adjust_period(int unit) const;
		uint8_t get_triangle_level() const;
		float get_triangle_frequency() const;
		uint8_t get_current_noise_bit() const;

		void update_period(int unit);

		//Frame Functionality
		void clock_envelope();
		void clock_length();
		void clock_sweep();
		void clock_linear_counter();
		int trigger_irq();
		void reset_frame();

		//Callback called by audio system
		void callback(unsigned short* buffer, int length);
		void generate_frame(int samples);
		void new_frame();
		int run_scanline(int line);
		void end_frame();
		unsigned int next_random_value();
	};
}