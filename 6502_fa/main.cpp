#pragma warning(disable : 4996)
//#define BOOST_ALL_DYN_LINK
#define _USE_MATH_DEFINES
#define WIN32_LEAN_AND_MEAN
#pragma comment(lib, "PowrProf.lib")

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <string>
#include <set>
#define GLEW_STATIC 
#include "GLew/glew.h"
#include <GL/glu.h>
#include <windows.h>
#include <mmsystem.h>
#include "glwindow.h"
#include "globj.h"
#include "shader.h"
#include "rasteriser.h"
#include "nes.h"
#include "audio.h"
#include "Powrprof.h"
#include "gamepad.h"
#include "framewaiter.h"
#include "consolefont.h"

using namespace std;

//Helper routines
static inline uint32_t rgba(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255) {
	return b | (g << 8) | (r << 16) | (a << 24);
}

//for debugging
static const size_t debug_x = 256 * 2;
static const size_t debug_y = 240 * 2;
static uint32_t video_debugbuf[debug_x * debug_y];
static uint32_t green = rgba(255, 194, 174);


static void pset(int x, int y, uint32_t c) {
	if (x < 0 || y < 0 || x >= debug_x|| y >= debug_y)
		return;
	video_debugbuf[x + y*debug_x] = c;
}

static const int font_height = 10;
static const int font_width = 8;
static void blit_char(unsigned char c, int px, int py) {
	for (int y = 0; y < 10; y++) {
		for (int x = 0; x < 8; x++) {
			auto bit = console_font_8x10[c * font_height + y] & (0x80 >> x);
			if (bit && (px + x) >= 0 && (px + x) < debug_x && (py + y) >= 0 && (py + y) < debug_y)
				pset(px + x, py + y, green);
		}
	}
}
static void print(const std::string line, int x, int y) {
	for (auto c : line) {
		blit_char(c, x, y);
		x += font_width;
	}
}

static void clear() {
	memset(video_debugbuf, 0, sizeof(video_debugbuf));
}

//ranges are h:[0,6] s:[0,1] v:[0,255]
void hsv2rgb_conversion(double h, double s, double v, GLubyte *colorvec) {
	int i = static_cast<int>(floor(h));
	double f = (i&1) ? (h-i) : (1.-h+i);
	GLubyte m = static_cast<GLubyte>(v * (1. - s));
	GLubyte n = static_cast<GLubyte>(v * (1. - s * f));
	int vlut[7] = {0,1,1,2,2,0,0};
	int nlut[7] = {1,0,2,1,0,2,1};
	int mlut[7] = {2,2,0,0,1,1,2};
	colorvec[vlut[i]] = static_cast<GLubyte>(v);
	colorvec[nlut[i]] = n;
	colorvec[mlut[i]] = m;

	return;
}

//is atan2 a suitable replacement?
double xy_to_degrees(double x, double y) {
	double h = sqrt(x*x + y*y);
	double angle;

	if (h == 0)
		return 0.0;
	if (y > 0)
		angle = acos(x/h);
	else if (x > 0)
		angle = (y>0) ? asin(y/h) : (2*M_PI+asin(y/h));
	else
		angle = M_PI - asin(y/h);
	return angle * 180.0 / M_PI;
}

namespace Input {
	bool keys[256];
	WPARAM buttons;
	POINTS mouse;
	
	void default_handler(unsigned char key) {}
	void (*key_up_handler)(unsigned char key) = default_handler;
	void (*key_down_handler)(unsigned char key) = default_handler;

	void init() {
		for(int i=0; i<sizeof(keys); i++)
			keys[i] = false;
		buttons = 0;
	}

	void keydown(WPARAM wParam) {
		//printf("Keydown:\t[%x]\n", wParam);
		keys[wParam & 0xff] = true;
		key_down_handler(wParam & 0xff);
	}

	void keyup(WPARAM wParam) {
		//printf("Keyup\t[%x]\n", wParam);
		keys[wParam & 0xff] = false;
		key_up_handler(wParam & 0xff);
	}

	void mousemove(WPARAM wParam, LPARAM lParam) {
		//printf("Mousemove\t[%x,%x]\n", wParam, lParam);
		buttons = wParam;
		mouse = MAKEPOINTS(lParam);
	}

	void mousedown(WPARAM wParam) {
		buttons = wParam;
	}

	void mouseup(WPARAM wParam) {
		buttons = wParam;
	}
}

namespace State {
	Nes::System nes;
	bool quit;
	GLWindow glwindow1;

	//32 tiles x 16 tiles
	int extra_cycle = 0;
	int frame = 0;
	bool debug = false;

	uint32_t nametable[256 * 2 * 240 * 2];

	void nes_input_keyboard(Nes::Controller* controller) {
		controller->up = Input::keys[VK_UP];
		controller->down = Input::keys[VK_DOWN];
		controller->left = Input::keys[VK_LEFT];
		controller->right = Input::keys[VK_RIGHT];
		controller->start = Input::keys[VK_RETURN];
		controller->b = Input::keys['Z'];
		controller->a = Input::keys['X'];
		controller->select = Input::keys['S'];
	}

	template<size_t num>
	void nes_input_gamepad(Nes::Controller* controller) {
		if (num < Input::gamepad.size()) {
			controller->up = Input::gamepad[num].left_y > .5;
			controller->down = Input::gamepad[num].left_y < -.5;
			controller->left = Input::gamepad[num].left_x < -.5;
			controller->right = Input::gamepad[num].left_x > .5;
			controller->start = Input::gamepad[num].button[8];
			controller->b = Input::gamepad[num].button[0];
			controller->a = Input::gamepad[num].button[1];
			//controller->b = Input::gamepad[0].button[1];
			//controller->a = Input::gamepad[0].button[4];
			controller->select = Input::gamepad[num].button[7];
		}
	}

	const int size_library = 89;
	const char* nes_library[size_library] = {
		"../GhostsNGoblins.nes",
		"../Castlevania.nes",
		"../Gradius.nes",
		"../Tetris_Tengen.nes",
		"../Recca.nes",
		"../RoadFighter.nes",
		"../instr_test/official_only.nes",
		"../SkyKid.nes",
		"../chuchu.nes",
		"../vbl_nmi_timing/1.frame_basics.nes",
		"../vbl_nmi_timing/2.vbl_timing.nes",
		"../vbl_nmi_timing/3.even_odd_frames.nes",
		"../vbl_nmi_timing/4.vbl_clear_timing.nes",
		"../vbl_nmi_timing/5.nmi_suppression.nes",
		"../vbl_nmi_timing/6.nmi_disable.nes",
		"../vbl_nmi_timing/7.nmi_timing.nes",
		"../metroid.nes",
		"../Bomberman.nes",
		"../smb3.nes",
		"../KirbysAdventure.nes",
		"../UrbanChampion.nes",
		"../Excitebike.nes",
		"../Popeye.nes",
		"../RiverCityRansom.nes",
		"../RCProAM.nes",
		"../Hostages.nes",
		"../HolyDiver.nes",
		"../KungFu.nes",
		"../ImageFight.nes",
		"../Tetris.nes",
		"../KidIcarus.nes",
		"../cpu_interrupts_v2/cpu_interrupts.nes",
		"../instr_misc/instr_misc.nes",
		"../blargg_ppu_test/vbl_clear_time.nes",
		"../blargg_ppu_test/palette_ram.nes",
		"../blargg_ppu_test/power_up_palette.nes",
		"../blargg_ppu_test/sprite_ram.nes",
		"../blargg_ppu_test/vram_access.nes",
		"../cpu_timing_test6/cpu_timing_test.nes",
		"../nestest2.nes",
		"../sprite0_test/07.screen_bottom.nes",
		"../sprite0_test/01.basics.nes",
		"../sprite0_test/02.alignment.nes",
		"../sprite0_test/03.corners.nes",
		"../sprite0_test/04.flip.nes",
		"../sprite0_test/05.left_clip.nes",
		"../sprite0_test/06.right_edge.nes",
		"../sprite0_test/08.double_height.nes",
		"../sprite0_test/09.timing_basics.nes",
		"../sprite0_test/10.timing_order.nes",
		"../sprite0_test/11.edge_timing.nes",
		"../SuperMarioBros(J).nes",
		"../smb2.nes",
		"../balloon.nes",
		"../donkey.nes",
		"../TinyToonAdventures.nes",
		"../Robocop2.nes",
		"../Robocop.nes",
		"../Megaman6.nes",
		"../NinjaGaiden.nes",
		"../MetalStorm.nes",
		"../Shatterhand.nes",
		"../LodeRunner.nes",
		"../FinalFantasy.nes",
		"../BlasterMaster.nes",
		"../contra.nes",
		"../Metalgear.nes",
		"../megaman.nes",
		"../1942.nes",
		"../smb.nes",
		"../SNDTEST.NES",
		"../NESTest.nes",
		"../NEStress.nes",
		"../apu_test/4015_cleared.nes",
		"../apu_test/4017_timing.nes",
		"../apu_test/4017_written.nes",
		"../apu_test/dmc.nes",
		"../apu_test/irq_flag_cleared.nes",
		"../apu_test/len_ctrs_enabled.nes",
		"../apu_test/min period.nes",
		"../apu_test/noise.nes",
		"../apu_test/square.nes",
		"../apu_test/sweep cutoff.nes",
		"../apu_test/sweep sub.nes",
		"../apu_test/triangle.nes",
		"../apu_test/volumes.nes",
		"../apu_test/works_immediately.nes",
		"../registers.nes",
		"../ram_after_reset.nes",
	};
	int cur_selected = 0;

	std::string sram_name() {
		std::string name = nes_library[cur_selected];
		name += std::string(".sram");
		return name;
	}

	bool file_exists(const std::string filename) {
		FILE *fp = fopen(filename.c_str(), "r");
		bool ret = fp != nullptr;
		if (fp)
			fclose(fp);
		return ret;
	}

	void load_sram() {
		char srambuf[0x2000];
		if (!(nes.cartridge.sram_enabled && file_exists(sram_name())))
			return;
		printf("Loading SRAM\n");
		FILE *fp = fopen(sram_name().c_str(), "rb");
		fread(srambuf, 0x2000, 1, fp);
		fclose(fp);
		memcpy(nes.cartridge.prg_ram, srambuf, 0x2000);
	}

	void save_sram() {
		if (!nes.cartridge.sram_enabled)
			return;
		printf("Saving SRAM\n");
		FILE *fp = fopen(sram_name().c_str(), "wb");
		fwrite(nes.cartridge.prg_ram, 0x2000, 1, fp);
		fclose(fp);
	}

	void load_cartridge() {
		if (nes.cartridge.loaded && nes.cartridge.sram_enabled)
			save_sram();
		printf("Loading cartridge...\n");
		nes.cartridge.load(nes_library[cur_selected]);
		nes.init();
		load_sram();
	}

	void key_up(unsigned char key) {
		if (key == 'D')
			debug = !debug;
		if (key == 'R')
			nes.reset();
		//'['
		if (key == VK_OEM_4) {
			cur_selected = (cur_selected + size_library - 1) % size_library;
			State::load_cartridge();
		}
		//']'
		if (key == VK_OEM_6) {
			cur_selected = (cur_selected + 1) % size_library;
			State::load_cartridge();
		}
	}

	void init() {
		extra_cycle = 0;
		quit = false;
		Input::init();
		Input::key_up_handler = key_up;
		nes.init();
		nes.controller[0].input_function = nes_input_gamepad<0>;
		nes.controller[1].input_function = nes_input_keyboard;
		load_cartridge();
		if (debug)
			nes.set_render_target(nametable);
		else
			nes.set_render_target(nullptr);
	}

	void shutdown() {
		save_sram();
	}

	int tile_x = 0;
	int tile_y = 0;

	void handle_mouse_click(int x, int y) {
		if (x < (256 * 2) && y < (240 * 2)) {
			tile_x = x >> 3;
			tile_y = y >> 3;
		}
	}

	uint32_t sprite0[16 * 8];
	uint8_t sprite0_height = 8;

	void update() {
		WPARAM buttons_edge_down, buttons_edge_up;
		static WPARAM prev_buttons = 0;

		buttons_edge_down = Input::buttons & ~prev_buttons;
		buttons_edge_up = Input::buttons & prev_buttons;
		prev_buttons = Input::buttons;

		//left mouse button down is button_edge_down & MK_LBUTTON

		if (Input::keys[VK_ESCAPE])
			State::quit = true;

		if (debug)
			nes.set_render_target(nametable);
		else
			nes.set_render_target(nullptr);

		extra_cycle = nes.run_frame(extra_cycle);
		++frame;

		clear();

		if (buttons_edge_down & WM_LBUTTONDOWN) {
			handle_mouse_click(Input::mouse.x, Input::mouse.y);
		}

		if (debug) {
			int name_address = nes.ppu.nametable_address(tile_x * 8, tile_y * 8);
			uint8_t tile_id = nes.ppu.read(name_address);
			auto current_line = 1;
			print("Tile:(" + to_string(tile_x) + ", " + to_string(tile_y) + ") " +
				"Address:" + to_string(name_address) + 
				" ID:" + to_string(tile_id), font_width, font_height * current_line++);
			
			for (size_t i = 0; i < 64; i++) {
				if (current_line == 16)
					break;
				if (nes.ppu.oam_attributes[i].y <= 239 || !i) {
					auto& n = nes.ppu.oam_attributes[i];
					print("OAM[" + to_string(i) + "] X:" + to_string(n.x) + " Y:" + to_string(n.y) + " Index:" + to_string(n.index) + " PAL:" + to_string(n.palette) + " P:" + to_string(n.priority), font_width, font_height * current_line++);
				}
			}
			nes.ppu.render_sprite(0, sprite0);
			sprite0_height = nes.ppu.ctrl.sprite_size ? 16 : 8;

			auto inv_nametable = [](int x, int y) {
				nametable[y * (256 * 2) + x] ^= 0xffffff00;
			};

			{
				int x = tile_x * 8;
				int y = tile_y * 8;
				for (int j = 0; j < 8; j++) {
					for (int i = 0; i < 8; i++) {
						inv_nametable(x + i, y + j);
					}
				}
			}
		}
	}

	void device_changed(HWND hWnd, WPARAM wParam, LPARAM lParam) {
		switch (wParam) {
		case DBT_DEVNODES_CHANGED:
			Input::refresh_devices(hWnd);
			break;
		default:
			break;
		}
	}

	bool extra_proc(UINT message, WPARAM wParam, LPARAM lParam) {
		switch (message) {
		case WM_DEVICECHANGE:
			device_changed(glwindow1.handle, wParam, lParam);
			return true;
		case WM_SYSCOMMAND:
			switch (wParam & 0xfff0) {
			case SC_MONITORPOWER:
			case SC_SCREENSAVE:
				return true;
			default:
				return false;
			}
			break;
		}

		return false;
	}
}

namespace Video {
	//Aspect ratio correction 256x240 -> 320x240 * 4
	int max_x = 256 * 5, max_y = 240 * 4;

	void setup3d_prespective() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		glFrustum(-1.0, 1.0, -1.0, 1.0, 1.0, 1000.0); 
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}

	void setup2d() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		gluOrtho2D(0, max_x, 0, max_y);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.375, 0.375, 0.0);
	}

	void setup() {
		glClearColor(0.0f, 0.0f, 0.0f, 1.0);
		glClearDepth(1.0);

		glViewport(0, 0, max_x, max_y);

		glFrontFace(GL_CCW);
		glCullFace(GL_BACK);
		glEnable(GL_CULL_FACE);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
		glEnable(GL_BLEND_COLOR);
		glShadeModel(GL_SMOOTH);

	}

	void render() {
		setup2d();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		if (State::debug) {
			glPixelZoom(2.0, -2.0);
			glWindowPos2f(0, Video::max_y - 240 * 2);
			glDrawPixels(256, 240, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, State::nes.ppu.output);
			
			glPixelZoom(1.0, -1.0);
			glWindowPos2f(0, Video::max_y);
			glDrawPixels(256 * 2, 240 * 2, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, State::nametable);

			glPixelZoom(5.0, -4.0);
			glWindowPos2f(512, Video::max_y - debug_y);
			glDrawPixels(8, State::sprite0_height, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, State::sprite0);

			glPixelZoom(1.0, -1.0);
			glWindowPos2f(512, Video::max_y);
			glDrawPixels(debug_x, debug_y, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, video_debugbuf);
		}
		else {
			glPixelZoom(5.0, -4.0);
			glWindowPos2f(0, Video::max_y);
			glDrawPixels(256, 240, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, State::nes.ppu.output);
		}
	}
}

typedef struct _PROCESSOR_POWER_INFORMATION {
	ULONG Number;
	ULONG MaxMhz;
	ULONG CurrentMhz;
	ULONG MhzLimit;
	ULONG MaxIdleState;
	ULONG CurrentIdleState;
} PROCESSOR_POWER_INFORMATION, *PPROCESSOR_POWER_INFORMATION;

void get_system_info() {
	SYSTEM_INFO system_info;
	GetSystemInfo(&system_info);
	int num_cpu = system_info.dwNumberOfProcessors;
	printf("%d CPUs detected.\n", system_info.dwNumberOfProcessors);
	std::vector<PROCESSOR_POWER_INFORMATION> caps;
	caps.resize(system_info.dwNumberOfProcessors);
	auto ret = CallNtPowerInformation(ProcessorInformation, NULL, 0, (void*)&caps[0], sizeof(PROCESSOR_POWER_INFORMATION)* num_cpu);
	int i = 1;
	for (auto& cpu_cap : caps) {
		printf("CPU[%d]: %d MHz\n", i++, cpu_cap.MaxMhz);
	}
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevIstance, LPSTR lpCmdLine, int nCmdShow)
{
	CreateConsole(1500);
	printf("Debug Console Online\n");

	get_system_info();
	
	State::glwindow1 = GLWindow(hInstance, "FinalNES", Video::max_x, Video::max_y);
	State::glwindow1.render_function = Video::render;
	State::glwindow1.keyup_function = Input::keyup;
	State::glwindow1.keydown_function = Input::keydown;
	State::glwindow1.mousemove_function = Input::mousemove;
	State::glwindow1.leftdown_function = Input::mousedown;
	State::glwindow1.leftup_function = Input::mouseup;
	State::glwindow1.proc_function = State::extra_proc;
	State::glwindow1.quit_function = []{State::quit = true;};
	State::glwindow1.init(Video::setup);

	Input::init(hInstance, State::glwindow1.handle);
	if (Input::gamepad.size()) {
		printf("Gamepad detected\n");
	}
	printf("Initialising state\n");
	State::init();

	State::glwindow1.show(nCmdShow);
	State::glwindow1.update();

	if (!Audio::init(State::glwindow1.handle, 48000, 6400)) {
		printf("Audio failed.\n");	
	}
	Audio::play();
	Timer::set_resolution(5);
	//Timer::create_timer(Audio::callback, NULL, 0, 5);

	int frame_count = 0;
	Frame_waiter frame_waiter(60);
	frame_waiter.start();
	DWORD prev_count = GetTickCount();

	MSG msg;
	InvalidateRect(State::glwindow1.handle, NULL, false);

	while(!State::quit) {
		for (; PeekMessage(&msg, State::glwindow1.handle, 0, 0, PM_REMOVE);) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if (msg.message == WM_QUIT) {
				State::quit = true;
				break;
			}
		}
		//else if (frame_waiter.wait_partial()) {
		if (frame_waiter.wait_partial()) {
			for (auto& pad : Input::gamepad)
				pad.update();
			State::update();
			InvalidateRect(State::glwindow1.handle, NULL, false);
			Audio::update_stream(State::nes.apu.buffer, 800);			
			//RedrawWindow(glwindow1.handle, NULL, NULL, RDW_UPDATENOW);
			
			//Frame rate calculation
			frame_count++;
			if (frame_count % frame_waiter.frame_rate == 0) {
				DWORD ticks = GetTickCount() - prev_count;
				float fps = 1000*frame_waiter.frame_rate/(float)ticks;
				//printf("FPS: %f\n", fps);
				prev_count = GetTickCount();
			}
		}
	}

	Audio::stop();

	Timer::reset_resolution();
	State::shutdown();
	FreeConsole();
	return 0;
}
