#ifndef AUDIO_H
#define AUDIO_H

#pragma once

#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "winmm.lib")
//#pragma comment(lib, "dxguid.lib")

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <InitGuid.h>
#include <mmsystem.h>
#include <dsound.h>
#include <cstdio>
#include <cstdint>

namespace Audio {
	extern IDirectSound8 *soundinterface;
	//State variables
	extern bool initialised;

	DWORD get_buffer_size();
	void get_buffer_position(DWORD* current_play, DWORD* current_write);
	int init(HWND hWnd, int sample_rate = 48000, int buffer_samples = 2048);
	void play();
	void stop();
	VOID CALLBACK callback(PVOID lpParam, BOOLEAN TimerOrWaitFired);
	void update_stream(const void *data, int samples);
	bool set_notifications(int num, uint32_t* positions, HANDLE* events);
}

#endif