#pragma once
#include <cstdint>

namespace Nes {
	struct System;

	// Picture Processing Unit, should interact with Video:: stuff
	// Note: Bit field is not exactly portable., when porting, you may have to
	// change the order a bit!
	struct Ppu {
		//OAM: Object Attributte Memory (sprite stuff)
		union {
			struct {
				uint8_t y;					//Top+1
				uint8_t index;				//Index in nametable to use
				struct {
					uint8_t palette : 2;
					uint8_t ignore : 3;
					uint8_t priority : 1;
					uint8_t flip_x : 1;
					uint8_t flip_y : 1;
				};
				uint8_t x;					//Left
			} oam_attributes[0x40];
			uint8_t oam[0x100];
		};

		//0x2000 - 0x2fff
		union {
			struct {
				uint8_t name[0x3c0];
				uint8_t attribute[0x40];
			};
			uint8_t byte[0x400];
		} table[4];

		union {
			struct {
				uint8_t image_palette[0x10];
				uint8_t sprite_palette[0x10];
			};
			uint8_t palette_memory[0x20];
		};

		//RGBA output
		unsigned int output[0x10000];		//256x240

		System& system;

		Ppu(System& system);

		//PPUCTRL $2000 (write)
		union {
			struct {
				uint8_t base_table : 2;			//------10 0:$2000,1:$2400,2:$2800,3:$2C00
				uint8_t vram_increment : 1;		//-----2-- 0:1 1:32 per read/write of PPUDATA
				uint8_t base_sprite : 1;		//----3--- 0:$0000,1:$1000
				uint8_t background_table : 1;	//---4---- 0:$0000,1:$1000
				uint8_t sprite_size : 1;		//--5----- 0:8x8,1:8x16
				uint8_t dummy : 1;				//-6------
				uint8_t enable_nmi : 1;			//7------- 0:off,1:on generate NMI
			};
			uint8_t byte;
		} ctrl;

		//PPUMASK $2001 (write)
		union {
			struct {
				uint8_t grayscale : 1;			//-------0 0:normal,1:and palette with 0x30
				uint8_t enable_bck_left : 1;	//------1- 0:off,1:on background leftmosk 8 pixels
				uint8_t enable_spr_left : 1;	//-----2-- 0:off,1:on sprite leftmosk 8 pixels
				uint8_t enable_background : 1;	//----3--- 0:off,1:on background rendering
				uint8_t enable_sprite : 1;		//---4---- 0:off,1:on sprite rendering
				uint8_t intensity_red : 1;		//--5----- }
				uint8_t intensity_green : 1;	//-6------ | TODO: Do later
				uint8_t intensity_blue : 1;		//7------- }
			};
			uint8_t byte;
		} mask;

		//PPUSTATUS $2002 (read)
		union {
			struct {
				uint8_t last_written : 5;
				uint8_t sprite_overflow : 1;	//--5----- 0:no,1:yes Sprite overflow
				uint8_t sprite0_hit : 1;		//-6------ 0:no,1:yes Sprite 0 hit
				uint8_t vblank_started : 1;		//7------- 0:no,1:yes Vertical blank started
				//note: reset when read.
			};
			uint8_t byte;
		} status;

		bool sprite0_hit_raised;

		//OAMADDR $2003 (write)
		unsigned char oam_address;

		//OAMDATA $2004 (read/write)
		//(no need for variable)

		//PPUSCROLL $2005 (write x 2)
		//PPUADDR $2006 (write x 2)
		union {
			struct {
				uint16_t x_coarse : 5;
				uint16_t y_coarse : 5;
				uint16_t base_table : 2;
				uint16_t y_fine : 3;
			};
			uint16_t data;
		} address, new_address;
		bool vram_toggle;
		uint8_t x_fine : 3;

		uint8_t get_xscroll() const;
		uint8_t get_yscroll() const;
		uint8_t get_basetable() const;
		void start_frame();
		void next_scanline();
		//void end_frame();
		void render_line(int line);

		//PPUDATA $2007 (read/write)
		unsigned char data;

		bool vint;
		void vblank();
		void vblank_end();

		void init();

		unsigned char read(unsigned short address);
		void write(unsigned short address, unsigned char data);

		unsigned short nametable_address(int x, int y) const;
		unsigned char get_attribute(unsigned short address);

		//Debug crap
		void render_name_table(uint32_t *target);
		void render_sprite(uint8_t sprite, uint32_t *target);
	};
}