#pragma once
#ifndef NES_H
#define NES_H
#include <cstdint>
#include "apu.h"
#include "ppu.h"
#include "mapper.h"

typedef unsigned char uint8;

namespace Nes {

// 1. immediate value: fetch next byte
// 2. absolute: fetch next two bytes (big endian like intel)
// 3. zeropage: fetch one byte, starts from 0
// 4. implied: (no operand required)
// 5. accumulator [implied]: (no operand required)
// 6. indexed: fetch next two bytes add to X or Y
// 7. zeropage indexed: fetch next byte and to (usually) X
// 8. indirect: jump to location given at an address (i.e. look it up first)
// 9. pre-indexed indirect: (zp) fetch a byte, add to X, look up the value in the next two bytes address
//10. post-indexed indirect: (zp) fetch a byte, look up value in address (two bytes), then add X, then look up
//11. relative: used in branches, adds to the PC.
enum Mode {
	imm = 1, abs, zp, imp, accum, ind_x, ind_y, zpind_x, zpind_y, indir, preindir, postindir, rel
};

//NON == NO such iNstruction (should halt)
//I'll get around to implementing undocumented opcodes if I test any games which need
//them.
enum Instruction {
	NON = 0, ADC, AND, ASL, BCC, BCS, BEQ, BIT, BMI, BNE, BPL, BRK, BVC, BVS, CLC,
	CLD, CLI, CLV, CMP, CPX, CPY, DEC, DEX, DEY, EOR, INC, INX, INY, JMP,
	JSR, LDA, LDX, LDY, LSR, NOP, ORA, PHA, PHP, PLA, PLP, ROL, ROR, RTI,
	RTS, SBC, SEC, SED, SEI, STA, STX, STY, TAX, TAY, TSX, TXA, TXS, TYA
};

extern const char opcode_mnemonic[][4];
extern const int cpu_info[151][5];
extern const int palette_rgb[64];
extern const char startup_palette[32];
//Forward declaration
struct System;

//Controller
struct Controller {
	union {
		struct {
			bool a, b;
			bool select, start;
			bool up, down, left, right;
		};
		bool state_register[8];
	};
	bool strobed;
	bool readout;
	int index;

	Controller();

	void (*input_function)(Controller* );
	static void default_get_input(Controller* );

	void init();
	void write(unsigned char data);
	unsigned char read();
};

// Basic loading/mapper info
struct Cartridge {
	bool loaded;

	bool vertical_mirroring;
	bool sram_enabled;
	bool trainer_present;
	bool four_screen;
	unsigned char numprg;
	typedef unsigned char PRG_Rom[0x4000];
	typedef unsigned char CHR_Rom[0x2000];
	PRG_Rom* prg_rom;
	CHR_Rom* chr_rom;
	unsigned char chr_ram[0x2000];
	unsigned char prg_ram[0x2000];
	unsigned char mapper_id;
	unsigned char numchr;
	Mapper* mapper = nullptr;

	int table_map[4];

	Cartridge();
	//Can't copy it
	Cartridge(const Cartridge& ) = delete;
	Cartridge& operator= (const Cartridge&) = delete;
	
	bool load(const char* filename);
	void unload();

	uint8 peek_prg(uint16_t address);
	uint8 read_prg(uint16_t address);
	void write_prg(uint16_t address, uint8_t data);
	uint8 read_chr(uint16_t address);
	void write_chr(uint16_t address, uint8_t data);
};

// Should implement hooks into mapper.
struct Memory {
	unsigned char cpu_ram[0x800];
	System* system;

	inline unsigned char read(unsigned short address);
	inline unsigned char read_io1(unsigned short address);
	inline unsigned char read_io2(unsigned short address);
	//inline unsigned char read_prgrom(unsigned short address);

	inline unsigned char peek(unsigned short address);
	inline unsigned char peek_io1(unsigned short address);
	inline unsigned char peek_io2(unsigned short address);
	inline unsigned char peek_prgrom(unsigned short address);

	inline void write(unsigned short address, unsigned char data);
	inline void write_io1(unsigned short address, unsigned char data);
	inline void write_io2(unsigned short address, unsigned char data);

	inline unsigned short read16(unsigned short address);
	inline void write16(unsigned short address, unsigned short data);
};

struct Cpu {
	unsigned short pc;
	unsigned char sp;
	unsigned char a, x, y;
	bool hung;
	bool debug;

	// 7:N 6:V 5:N/A 4:B 3:D 2:I 1:Z 0:C
	static const unsigned char	FLAG_N = 0x80,	//Sign (negative) flag
								FLAG_V = 0x40,	//Overflow flag
								FLAG_B = 0x10,	//
								FLAG_D = 0x08,	//Decimal flag (ignored)
								FLAG_I = 0x04,	//Interrupt	disable
								FLAG_Z = 0x02,	//Zero flag
								FLAG_C = 0x01;	//Carry flag
	unsigned char f;

	Memory memory;
	System& system;

	unsigned char opcode_cycles[256];
	Mode opcode_mode[256];
	unsigned char opcode_operandsize[256];
	Instruction opcode_instruction[256];
	
	unsigned short dma_page;
	bool dma_requested;

	//cycle adjustment based on page crossing/branches
	int delta_cycles;

	Cpu(System& system);

	void power();
	void init();
	void reset();
	void push(unsigned char data);
	unsigned char pop();

	int nmi();
	int irq();
	int execute(int required_cycles);
	inline unsigned short fetch_target(Mode mode);
	inline unsigned short fetch_operand(Mode mode);

	//For debugging
	inline unsigned short fetch_parameter(Mode mode);

	int execute_opcode(unsigned char opcode);

	void debug_out_opcode(unsigned char opcode);

	inline void set_sign(unsigned char data);
	inline void set_overflow(unsigned char data);
	//...
	inline void set_break(unsigned char data);
	inline void set_decimal(unsigned char data);
	inline void set_interrupt(unsigned char data);
	inline void set_zero(unsigned char data);
	inline void set_carry(unsigned char data);

	//wrap around bug
	inline uint16_t zp_read16(unsigned char zp_address);

	//branch and adjust cycles
	inline void branch(Mode mode);

	// ADC, AND, ASL, BCC, BCS, BEQ, BIT, BMI, BNE, BPL, BRK, BVC, BVS, CLC,
	// CLD, CLI, CLV, CMP, CPX, CPY, DEC, DEX, DEY, EOR, INC, INX, INY, JMP,
	// JSR, LDA, LDX, LDY, LSR, NOP, ORA, PHA, PHP, PLA, PLP, ROL, ROR, RTI,
	// RTS, SBC, SEC, SED, SEI, STA, STX, STY, TAX, TAY, TSX, TXA, TXS, TYA
	inline void opADC(Mode mode);
	inline void opAND(Mode mode);
	inline void opASL(Mode mode);
	inline void opBCC(Mode mode);
	inline void opBCS(Mode mode);
	inline void opBEQ(Mode mode);
	inline void opBIT(Mode mode);
	inline void opBMI(Mode mode);
	inline void opBNE(Mode mode);
	inline void opBPL(Mode mode);
	inline void opBRK(Mode mode);
	inline void opBVC(Mode mode);
	inline void opBVS(Mode mode);
	inline void opCLC(Mode mode);
	inline void opCLD(Mode mode);
	inline void opCLI(Mode mode);
	inline void opCLV(Mode mode);
	inline void opCMP(Mode mode);
	inline void opCPX(Mode mode);
	inline void opCPY(Mode mode);
	inline void opDEC(Mode mode);
	inline void opDEX(Mode mode);
	inline void opDEY(Mode mode);
	inline void opEOR(Mode mode);
	inline void opINC(Mode mode);
	inline void opINX(Mode mode);
	inline void opINY(Mode mode);
	inline void opJMP(Mode mode);
	inline void opJSR(Mode mode);
	inline void opLDA(Mode mode);
	inline void opLDX(Mode mode);
	inline void opLDY(Mode mode);
	inline void opLSR(Mode mode);
	inline void opNOP(Mode mode);
	inline void opORA(Mode mode);
	inline void opPHA(Mode mode);
	inline void opPHP(Mode mode);
	inline void opPLA(Mode mode);
	inline void opPLP(Mode mode);
	inline void opROL(Mode mode);
	inline void opROR(Mode mode);
	inline void opRTI(Mode mode);
	inline void opRTS(Mode mode);
	inline void opSBC(Mode mode);
	inline void opSEC(Mode mode);
	inline void opSED(Mode mode);
	inline void opSEI(Mode mode);
	inline void opSTA(Mode mode);
	inline void opSTX(Mode mode);
	inline void opSTY(Mode mode);
	inline void opTAX(Mode mode);
	inline void opTAY(Mode mode);
	inline void opTSX(Mode mode);
	inline void opTXA(Mode mode);
	inline void opTXS(Mode mode);
	inline void opTYA(Mode mode);

};

struct System {
	Cpu cpu;
	Ppu ppu;
	Apu apu;
	Controller controller[2];
	Cartridge cartridge;

	static const int ntsc_frequency = 1789773;
	//Get around the fact that the scanline are 113.67 CPU cycles long
	int fractional_cycle;

	System();
	void init();
	void reset();
	int run_scanline(int extra_cycles);
	int run_frame(int extra_cycles);
	
	//Debug nametable render target
	uint32_t *nametable_buffer = nullptr;
	void set_render_target(uint32_t *target);
};
}

#endif
