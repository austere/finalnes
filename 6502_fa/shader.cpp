#define GLEW_STATIC 
#include "GLew/glew.h"
#include <windows.h>
#include <vector>
#include <fstream>
#include <string>
#include "shader.h"

using namespace std;

Shader::Shader() : program(0) {}
Shader::~Shader() {
	if (program)
		glDeleteProgram(program);
	for (auto& e : vertex)
		glDeleteShader(e);
	for (auto& e : fragment)
		glDeleteShader(e);
}

bool Shader::readfile(const char *filename) {
	ifstream fp(filename);
	
	if (!fp.is_open() || fp.bad())
		return false;

	cur_file_content.clear();
	for (string line;!getline(fp, line).eof();) {
		cur_file_content += line;
		cur_file_content += "\n";
	}
	fp.close();
	return true;
}

bool Shader::add_shader_file(const char *filename, GLenum type) {
	if (!readfile(filename)) {
		log += "failed to open shader ";
		log += filename;
		log += "\n";
		return false;
	}
	GLuint cur_shader = glCreateShader(type);

	const char *file_content = cur_file_content.c_str();
	glShaderSource(cur_shader, 1, &file_content, NULL);
	cur_file_content.clear();

	glCompileShader(cur_shader);
	append_shader_log(cur_shader);

	GLint ret;
	glGetShaderiv(cur_shader, GL_COMPILE_STATUS, &ret);
	if (ret == GL_TRUE)
		vertex.push_back(cur_shader);
	else
		glDeleteShader(cur_shader);
	return (ret == GL_TRUE);
}

bool Shader::add_shader(const char *src, GLenum type) {
	GLuint cur_shader = glCreateShader(type);

	glShaderSource(cur_shader, 1, &src, NULL);
	cur_file_content.clear();

	glCompileShader(cur_shader);
	append_shader_log(cur_shader);

	GLint ret;
	glGetShaderiv(cur_shader, GL_COMPILE_STATUS, &ret);
	if (ret == GL_TRUE)
		vertex.push_back(cur_shader);
	else
		glDeleteShader(cur_shader);
	return (ret == GL_TRUE);

}

bool Shader::add_vertex(char *filename) {
	return add_shader(filename, GL_VERTEX_SHADER);
}

bool Shader::add_fragment(char *filename) {
	return add_shader(filename, GL_FRAGMENT_SHADER);
}

bool Shader::add_geometry(char *filename) {
	return add_shader(filename, GL_GEOMETRY_SHADER);
}

void Shader::append_shader_log(GLuint shader) {
	int shader_log_length;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &shader_log_length);
	if (!shader_log_length)
		return;
	char* log_temp = new char[shader_log_length];
	glGetShaderInfoLog(shader, shader_log_length, NULL, log_temp);
	log += log_temp;
	delete [] log_temp;
}

void Shader::append_program_log() {
	int program_log_length;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &program_log_length);
	if (!program_log_length)
		return;
	char* log_temp = new char[program_log_length];
	glGetProgramInfoLog(program, program_log_length, NULL, log_temp);
	log += log_temp;
	delete [] log_temp;
}

bool Shader::link() {
	if (program)
		glDeleteProgram(program);
	program = glCreateProgram();
	for(size_t i=0, max_vert = vertex.size(); i < max_vert; i++)
		glAttachShader(program, vertex[i]);
	for(size_t i=0, max_frag = fragment.size(); i < max_frag; i++)
		glAttachShader(program, fragment[i]);
	glLinkProgram(program);
	append_program_log();

	int linked;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE) {
		glDeleteProgram(program);
		program = 0;
	}

	return linked == GL_TRUE;
}

void Shader::use() {
	glUseProgram(program);
}

void Shader::unuse() {
	glUseProgram(0);
}