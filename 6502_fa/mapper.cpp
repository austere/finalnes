#pragma warning(disable: 4800)
#include "mapper.h"
#include "nes.h"
#include <cstdio>
#include <cstdlib>
#include <utility>

namespace Nes {
	Mapper* make_mapper(Cartridge& cartridge, uint8_t mapper_id) {
		switch (mapper_id) {
		default:
			printf("UNKNOWN MAPPER %d\n", mapper_id);
			::system("PAUSE");
		//Fucking Discdude
		case 64:
		case 0:
			return new Mapper0(cartridge);
		//MMC1
		case 1:
			return new Mapper1(cartridge);
		//UNROM (Contra etc)
		case 2:
			return new Mapper2(cartridge);
		//CNROM
		case 3:
			return new Mapper3(cartridge);
		//MMC3
		case 4:
			return new Mapper4(cartridge);
		case 78:
			return new Mapper78(cartridge);
		}
	}

	static enum class Mirror { HORIZONTAL = 0, VERTICAL, SINGLE_A, SINGLE_B, FOUR_SCREEN };
	static void set_mirroring(Cartridge& cartridge, Mirror mirror_type) {
		int *table_map = cartridge.table_map;
		switch (mirror_type) {
		case Mirror::SINGLE_A:
			table_map[0] = table_map[1] = 0;
			table_map[2] = table_map[3] = 0;
			break;
		case Mirror::SINGLE_B:
			table_map[0] = table_map[1] = 1;
			table_map[2] = table_map[3] = 1;
			break;
		case Mirror::VERTICAL:
			table_map[0] = table_map[2] = 0;
			table_map[1] = table_map[3] = 1;
			break;
		case Mirror::HORIZONTAL:
			table_map[0] = table_map[1] = 0;
			table_map[2] = table_map[3] = 1;
			break;
		case Mirror::FOUR_SCREEN:
			for (int i = 0; i < 4; i++)
				table_map[i] = i;
			break;
		}

	}

	//============================================================================
	//Mapper::
	//============================================================================

	//Default mapper hsync returns true if needs irq
	bool Mapper::hsync(int line) {
		return false;
	}
	
	//============================================================================
	//Mapper0::
	//============================================================================
	Mapper0::Mapper0(Cartridge& cartridge) : cartridge(cartridge) {
	}

	void Mapper0::reset() {
		prg_bank[0] = cartridge.prg_rom[0];
		if (cartridge.numprg > 1)
			prg_bank[1] = cartridge.prg_rom[1];
		else
			prg_bank[1] = cartridge.prg_rom[0];

		if (cartridge.numchr) {
			chr_bank = cartridge.chr_rom[0];
			chr_writable = false;
		}
		else {
			chr_bank = cartridge.chr_ram;
			chr_writable = true;
		}
	}

	uint8_t Mapper0::read_prg(uint16_t address) {
		return peek_prg(address);
	}

	uint8_t Mapper0::peek_prg(uint16_t address) const {
		return prg_bank[((address & 0x4000) >> 14) & 0x1][address & 0x3fff];
	}

	void Mapper0::write_prg(uint16_t address, uint8_t data) {

	}

	uint8_t Mapper0::read_chr(uint16_t address) {
		return chr_bank[address & 0x1fff];
	}

	void Mapper0::write_chr(uint16_t address, uint8_t data) {
		if (chr_writable)
			cartridge.chr_ram[address & 0x1fff] = data;
	}

	uint8_t Mapper0::read_prgram(uint16_t address) {
		return 0;
	}

	uint8_t Mapper0::peek_prgram(uint16_t address) {
		return 0;
	}

	void Mapper0::write_prgram(uint16_t address, uint8_t data) {
	}

	//============================================================================
	//Mapper1::
	//============================================================================
	Mapper1::Mapper1(Cartridge& cartridge) : cartridge(cartridge) {
	}

	void Mapper1::update() {
		if (uses_chr_ram) {
			if (chr_mode) {
				chr_bank[0] = cartridge.chr_ram + ((chr_register0 & 0x1) << 12);
				chr_bank[1] = cartridge.chr_ram + ((chr_register1 & 0x1) << 12);
			}
			else {
				chr_bank[0] = cartridge.chr_ram;
				chr_bank[1] = cartridge.chr_ram + 0x1000;
			}

		}
		//TODO: Bounds check
		else {
			if (chr_mode) {
				chr_bank[0] = cartridge.chr_rom[chr_register0 >> 1] + ((chr_register0 & 0x1) ? 0x1000 : 0x0);
				chr_bank[1] = cartridge.chr_rom[chr_register1 >> 1] + ((chr_register1 & 0x1) ? 0x1000 : 0x0);
			}
			else {
				chr_bank[0] = cartridge.chr_rom[chr_register0 >> 1];
				chr_bank[1] = cartridge.chr_rom[chr_register0 >> 1] + 0x1000;
			}
		}

		if (prg_size) {
			if (slot_select) {
				prg_bank[0] = cartridge.prg_rom[prg_register & 0xf];
				//Page {$0F}?
				prg_bank[1] = cartridge.prg_rom[cartridge.numprg - 1];
			}
			else {
				//Page {$0}
				prg_bank[0] = cartridge.prg_rom[0];
				prg_bank[1] = cartridge.prg_rom[prg_register & 0xf];
			}
		}
		else {
			prg_bank[0] = cartridge.prg_rom[(prg_register & ~0x1)];
			prg_bank[1] = cartridge.prg_rom[(prg_register & ~0x1) | 0x1];
		}

		int *table_map = cartridge.table_map;
		switch (mirror_control) {
		case 0:
			set_mirroring(cartridge, Mirror::SINGLE_A);
			break;
		case 1:
			set_mirroring(cartridge, Mirror::SINGLE_B);
			break;
		case 2:
			set_mirroring(cartridge, Mirror::VERTICAL);
			break;
		case 3:
			set_mirroring(cartridge, Mirror::HORIZONTAL);
			break;
		}
	}

	void Mapper1::reset() {
		register_0 = 0;
		chr_register0 = 0;
		chr_register1 = 0;
		register_3 = 0;
		uses_chr_ram = cartridge.numchr == 0;
		reset_shift_register();
	}

	void Mapper1::reset_shift_register() {
		count = 0;
		shift_register = 0;
		register_0 |= 0xc;
		update();
	}

	uint8_t Mapper1::read_prg(uint16_t address) {
		return peek_prg(address);
	}

	uint8_t Mapper1::peek_prg(uint16_t address) const {
		return prg_bank[((address & 0x4000) >> 14) & 0x1][address & 0x3fff];
	}

	void Mapper1::write_prg(uint16_t address, uint8_t data) {
		if (data & 0x80) {
			reset_shift_register();
			return;
		}
		shift_register |= (data & 0x1) << (count++);
		if (count != 5)
			return;
		count = 0;

		//Register 0xAxxx
		switch (address >> 12) {
		case 0x8:
		case 0x9:
			register_0 = shift_register;
			//printf("Mode changed: %d\n", register_0);
			break;
		case 0xa:
		case 0xb:
			chr_register0 = shift_register;
			//printf("CHR0 Changed: %d\n", chr_register0);
			break;
		case 0xc:
		case 0xd:
			chr_register1 = shift_register;
			//printf("CHR1 Changed: %d\n", chr_register1);
			break;
		case 0xe:
		case 0xf:
			register_3 = shift_register;
			//printf("PRG Changed: %d\n", prg_register);
			break;
		default:
			printf("MAPPER ERROR\n");
			exit(-1);
		}
		shift_register = 0;
		update();
	}
	
	uint8_t Mapper1::read_chr(uint16_t address) {
		return chr_bank[(address >> 12) & 0x1][address & 0xfff];
	}

	void Mapper1::write_chr(uint16_t address, uint8_t data) {
		//if (uses_chr_ram)
		//	chr_bank[(address >> 12) & 0x1][address & 0xfff] = data;
		cartridge.chr_ram[address & 0x1fff] = data;
	}

	uint8_t Mapper1::read_prgram(uint16_t address) {
		return peek_prgram(address);
	}

	uint8_t Mapper1::peek_prgram(uint16_t address) {
		return cartridge.prg_ram[address & 0x1fff];
	}

	void Mapper1::write_prgram(uint16_t address, uint8_t data) {
		cartridge.prg_ram[address & 0x1fff] = data;
	}
	
	//============================================================================
	//Mapper2::
	//============================================================================
	Mapper2::Mapper2(Cartridge& cartridge) : Mapper0(cartridge) {
	}

	void Mapper2::switch_bank(int data) {
		//printf("Switched to bank %d\n", data);
		if (cartridge.numprg < 16)
			cur_bank = data & 0x7;
		else
			cur_bank = data & 0xf;
		prg_bank[0] = cartridge.prg_rom[cur_bank];
	}

	void Mapper2::reset() {
		//Write directly to CHR RAM
		chr_bank = cartridge.chr_ram;
		switch_bank(0);
		//$c000-$ffff set fixed to last bank
		prg_bank[1] = cartridge.prg_rom[cartridge.numprg - 1];
	}

	void Mapper2::write_prg(uint16_t address, uint8_t data) {
		switch_bank(data);
	}

	void Mapper2::write_chr(uint16_t address, uint8_t data) {
		cartridge.chr_ram[address & 0x1fff] = data;
	}

	//============================================================================
	//Mapper3::
	//============================================================================
	Mapper3::Mapper3(Cartridge& cartridge) : Mapper0(cartridge) {

	}

	void Mapper3::reset() {
		Mapper0::reset();
		cur_bank = 0;
		update_bank();
	}

	void Mapper3::update_bank() {
		if (cartridge.numchr < 4)
			chr_bank = cartridge.chr_rom[cur_bank & 0x1];
		else
			chr_bank = cartridge.chr_rom[cur_bank & 0x3];
	}

	void Mapper3::write_prg(uint16_t address, uint8_t data) {
		cur_bank = data & 0x3;
		update_bank();
	}


	//============================================================================
	//Mapper4::
	//============================================================================
	Mapper4::Mapper4(Cartridge& cartridge) : cartridge(cartridge) {
	}

	void Mapper4::reset() {
		for (int i = 0; i < 8; i++)
			bank_register[i] = 0;
		bank_select_register = 0;
		horizontal_mirroring = false;
		chip_enabled = true;
		write_protect = false;
		irq_enabled = false;
		irq_reload = 1;
		irq_counter = 0;
		update();
	}

	void Mapper4::update() {
		//TODO: Proper bound check
		auto prg_select = [&](int select) {
			if (cartridge.numprg < 16)
				select &= 0x7;
			else if (cartridge.numprg < 32)
				select &= 0xf;
			else if (cartridge.numprg < 64)
				select &= 0x1f;
			return cartridge.prg_rom[select];
		};
		//PRG space
		prg_bank[0] = prg_select(prg_selected[0] >> 1) + ((prg_selected[0] & 0x1) ? 0x2000 : 0x0);
		prg_bank[1] = prg_select(prg_selected[1] >> 1) + ((prg_selected[1] & 0x1) ? 0x2000 : 0x0);
		prg_bank[2] = cartridge.prg_rom[cartridge.numprg - 1];
		prg_bank[3] = cartridge.prg_rom[cartridge.numprg - 1] + 0x2000;
		if (prg_mode)
			std::swap(prg_bank[0], prg_bank[2]);

		auto chr_select = [&](int select) {
			auto offset = (select & 0x7) << 10;
			if (cartridge.numchr)
				return cartridge.chr_rom[select >> 3] + offset;
			else
				return cartridge.chr_ram + offset;
		};

		//CHR space
		chr_bank[0] = chr_select(chr_selected[0] & ~0x1);
		chr_bank[1] = chr_select(chr_selected[0] | 0x01);
		chr_bank[2] = chr_select(chr_selected[1] & ~0x1);
		chr_bank[3] = chr_select(chr_selected[1] | 0x01);
		chr_bank[4] = chr_select(chr_selected[2]);
		chr_bank[5] = chr_select(chr_selected[3]);
		chr_bank[6] = chr_select(chr_selected[4]);
		chr_bank[7] = chr_select(chr_selected[5]);
		if (chr_invert) {
			for (int i = 0; i < 4; i++)
			std::swap(chr_bank[i], chr_bank[i + 4]);
		}

		//Mirroring
		if (horizontal_mirroring)
			set_mirroring(cartridge, Mirror::HORIZONTAL);
		else
			set_mirroring(cartridge, Mirror::VERTICAL);
	}

	bool Mapper4::hsync(int line) {
		bool ret = false;
		if (!irq_counter) {
			irq_counter = irq_reload;
		}
		else if (!(--irq_counter)) {
			ret = irq_enabled;
		}
		return ret;
	}

	uint8_t Mapper4::read_prg(uint16_t address) {
		return peek_prg(address);
	}

	uint8_t Mapper4::peek_prg(uint16_t address) const {
		return prg_bank[(address >> 13) & 0x3][address & 0x1fff];
	}

	void Mapper4::write_prg(uint16_t address, uint8_t data) {
		auto selector = (address >> 12) & 0xe | (address & 0x1);
		switch (selector) {
		//Bank select ($8000-$9FFE) Even
		case 0x8:
			bank_select_register = data;
			update();
			break;
		//Bank data ($8001-$9FFF) Odd
		case 0x9:
			bank_register[bank_selected] = data;
			//printf("Bank[%d] = %d\n", bank_selected, data);
			update();
			break;
		//Mirroring ($A000-$BFFE) Even
		case 0xa:
			horizontal_mirroring = data & 0x1;
			update();
			break;
		//PRG RMA protect ($A001-$BFFF) Odd
		case 0xb:
			chip_enabled = data & 0x80;
			write_protect = data & 0x40;
			break;
		case 0xc:
			irq_reload = data;
			//printf("Reload value set: %d\n", data);
			break;
		case 0xd:
			irq_counter = 0;
			//printf("IRQ counter reset\n", data);
			break;
		case 0xe:
			irq_enabled = false;
			//Acknowledge pending interrupt
			//printf("IRQ Disabled\n", data);
			break;
		case 0xf:
			irq_enabled = true;
			//printf("IRQ Enabled\n", data);
			break;
		default:
			printf("MAPPER ERROR\n");
			exit(-1);
		}

	}

	uint8_t Mapper4::read_chr(uint16_t address) {
		return chr_bank[(address >> 10) & 0x7][address & 0x3ff];
	}

	void Mapper4::write_chr(uint16_t address, uint8_t data) {
		if (!cartridge.numchr) {

			chr_bank[(address >> 10) & 0x7][address & 0x3ff] = data;
		}
	}

	uint8_t Mapper4::read_prgram(uint16_t address) {
		return peek_prgram(address);
	}

	uint8_t Mapper4::peek_prgram(uint16_t address) {
		return cartridge.prg_ram[address & 0x1fff];
	}

	void Mapper4::write_prgram(uint16_t address, uint8_t data) {
		cartridge.prg_ram[address & 0x1fff] = data;
	}

	//============================================================================
	//Mapper78::
	//============================================================================

	Mapper78::Mapper78(Cartridge& cartridge) : Mapper0(cartridge) {
	}

	//TODO: add bounds checking
	void Mapper78::update() {
		prg_bank[0] = cartridge.prg_rom[cur_prg_bank];
		prg_bank[1] = cartridge.prg_rom[cartridge.numprg - 1];
		chr_bank = cartridge.chr_rom[cur_chr_bank];
		set_mirroring(cartridge, mirror_mode ? Mirror::VERTICAL : Mirror::HORIZONTAL);
	}

	void Mapper78::reset() {
		mirror_mode = false;
		cur_chr_bank = 0;
		cur_prg_bank = 0;
		update();
	}

	void Mapper78::write_prg(uint16_t address, uint8_t data) {
		cur_prg_bank = data & 0x7;
		cur_chr_bank = data >> 4;
		mirror_mode = (data >> 3) & 0x1;
		update();
	}
}
