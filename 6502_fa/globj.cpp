#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <map>
#include <cstring>
#include "globj.h"
#include "GLee/glee.h"

using namespace std;

Point3::Point3() {}
Point3::Point3(float x, float y, float z) : x(x), y(y), z(z) {}
Point3::Point3(string& s, size_t offset) {
	istringstream ss(s);
	ss.seekg(streamoff(offset));

	if (!(ss >> x)) {
		x = 0.f;
		y = 0.f;
		z = 0.f;
	}
	else {
		if (ss >> y) {
			if (!(ss >> z))
				z = 0.f;
		}
		else {
			y = 0.f;
			z = 0.f;
		}
	}
}

FaceRef::FaceRef() {}
FaceRef::FaceRef(int v, int vt, int vn) : v(v), vt(vt), vn(vn) {}

Object::Object() {}

Object::Object(char* filename) {
	parse_file(filename);	
}

void Object::clear() {
	v.clear();
	vt.clear();
	vn.clear();
}

const char* whitespace = " \t";

int Object::parse_file(char* filename) {
	clear();
	ifstream fp(filename);
	if (!fp.is_open())
		return -1;
	
	for (string line;!getline(fp, line).eof();) {
		size_t curpos = line.find_first_not_of(whitespace);
		if (curpos == string::npos || line[curpos] == '#')
			continue;
		size_t nextpos = line.find_first_of(whitespace, curpos);
		string command = line.substr(curpos, nextpos-curpos);
		curpos = line.find_first_not_of(whitespace, nextpos);
		string parameter = line.substr(curpos);

		if (command == "v")
			v.push_back(Point3(parameter));
		else if (command == "vt")
			vt.push_back(Point3(parameter));
		else if (command == "vn")
			vn.push_back(Point3(parameter));
		else if (command == "f") {
			Face f;
			if (parse_face(f, parameter))
				face.push_back(f);
		}
		//else if (command == "mtllib") {}
		//else if (command == "usemtl") {}
	}

	fp.close();
	return 0;
}

bool Object::parse_face(Face& f, string& s) {
	const char* param = s.c_str();

	//TODO: non-sketchup file.
	if (s.find_first_of("/") == s.npos)
		return false;

	int v, vt, vn;
	for(int offset;; param += offset) {
		int ret = sscanf(param, "%d%/%d%/%d %n", &v, &vt, &vn, &offset);
		if (ret != 3)
			break;
		f.push_back(FaceRef(v - 1, vt - 1, vn - 1));
	}

	return true;
}

void Object::render() {
	for(size_t facenum=0, totalface = face.size(); facenum < totalface; facenum++) {
		switch(face[facenum].size()) {
		case 3:
			glBegin(GL_TRIANGLES);
			break;
		case 4:
			glBegin(GL_QUADS);
			break;
		default:
			glBegin(GL_POLYGON);
			break;
		}
		
		for (size_t i=0, total = face[facenum].size(); i < total; i++) {
			Point3 _v = v[face[facenum][i].v] ;
			Point3 _vn = vn[face[facenum][0].vn] ;
			Point3 _vt = vt[face[facenum][i].vt] ;

			glNormal3f(_vn.x, _vn.y, _vn.z);
			glTexCoord2f(_vt.x, _vt.y);
			glVertex3f(_v.x, _v.y, _v.z);
		}
		glEnd();
	}
}

void normal_colour(Point3& fv) {
	if (fabs(fv.x) > fabs(fv.y)) {
		if (fabs(fv.x) > fabs(fv.z))
			glColor3f(1.0, 0.2f, 0.2f);
		else
			glColor3f(0.2f, 0.2f, 1.0);
	}
	else {
		if (fabs(fv.y) > fabs(fv.z))
			glColor3f(0.2f, 1.0, 0.2f);
		else
			glColor3f(0.2f, 0.2f, 1.0);
	}
}

void Object::render_normals() {
	glBegin(GL_LINES);
	for(size_t facenum=0, totalface = face.size(); facenum < totalface; facenum++) {
		for (size_t i=0, total = face[facenum].size(); i < total; i++) {
			Point3 _v = v[face[facenum][i].v] ;
			Point3 _vn = vn[face[facenum][i].vn] ;
			
			normal_colour(_vn);
			glVertex3f(_v.x, _v.y, _v.z);
			glVertex3f(_v.x + _vn.x, 
					   _v.y + _vn.y, 
					   _v.z + _vn.z);
		}
	}
	glEnd();
}