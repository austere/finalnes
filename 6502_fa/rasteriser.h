//Bit Blit Rasteriser
//(bbr)
//by abionnnn

//#include "sg.h"
#ifndef RASTERISER_H
#define RASTERISER_H

#define sgFloat float
#define SGfloat float

typedef SGfloat sgVec2[2];

typedef __int32 Bitmap32[32];

namespace bbr {
	extern const unsigned int bm_span_all[33];
	extern const unsigned int* bm_span;

    struct Triangle {
        float x1, y1;
        float x2, y2;
        float x3, y3;

        Triangle(float x1, float y1, 
                 float x2, float y2,
                 float x3, float y3);

        void sort();
		void scale(sgVec2 scale);

		void offset(sgVec2 offset);

	private:
        inline bool vertex_lt(float x1, float y1, float x2, float y2);
    };

    void draw_Triangle(Bitmap32& bm, Triangle t, sgVec2 scale, sgVec2 offset);
    void draw_topTriangle(Bitmap32& bm, Triangle& t);
    void draw_bottomTriangle(Bitmap32& bm, Triangle& t);
}

#endif