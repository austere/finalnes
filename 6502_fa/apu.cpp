#include <memory>
#include <string>
#include "nes.h"

using namespace std;

namespace Nes {

	//============
	//Apu::
	// Part of the CPU, but we split it up for neatness. Hooked to memory
	Apu::Apu(System& system) : system(system) {
		init();
	}

	void Apu::init() {
		frame_control.disable_irq = 1;
		memset(buffer, 0, 800 * 4);
		for (int i = 0; i < 2; i++) {
			//Pulse
			pulse_ctrl[i].byte = 0;
			pulse_sweep[i].byte = 0;
			pulse_period_low[i] = 0;
			pulse_coarse[i].byte = 0;
			length_count[i] = 0;
			pulse_phase[i] = 0;
			envelope_start_flag[i] = false;
			sweep_reload_flag[i] = false;
		}
		//Triangle
		linear_counter = 0;
		length_count[2] = 0;
		triangle_phase = 0;
		triangle_ctrl.byte = 0;
		triangle_period_low = 0;
		triangle_coarse.byte = 0;
		linear_reload_flag = false;
		triangle_halt_flag = false;
		master_control.byte = 0;
		//Noise
		lsfr_reg = 1;
		noise_fractional_sample = 0;
		noise_ctrl.byte = 0;
		noise_coarse.byte = 0;
		noise_envelope = 0;
		noise_envelope_divider = 0;
		length_count[3] = 0;
		noise_start_flag = false;
	}

	//Noise channel's duration table used by length index to set a tone length
	const unsigned char Apu::duration_table[0x20] = {
		0x0a, 0xfe, 0x14, 0x02, 0x28, 0x04, 0x50, 0x06,
		0xa0, 0x08, 0x3c, 0x0a, 0x0e, 0x0c, 0x1a, 0x0e,
		0x0c, 0x10, 0x18, 0x12, 0x30, 0x14, 0x60, 0x16,
		0xc0, 0x18, 0x48, 0x1a, 0x10, 0x1c, 0x20, 0x1e
	};

	//duty cycle table
	//0 = _-______
	//1 = _--_____
	//2 = _----___
	//3 = -__-----
	const unsigned char Apu::duty_table[4] = { 0x02, 0x06, 0x1e, 0xfa };

	const unsigned char Apu::triangle_table[32] =
	{ 0xf, 0xe, 0xd, 0xc, 0xb, 0xa, 0x9, 0x8, 0x7, 0x6, 0x5, 0x4, 0x3, 0x2, 0x1, 0x0,
	0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf };

	const unsigned short Apu::noise_period_table[16] =
	{ 4, 8, 16, 32, 64, 96, 128, 160, 202, 254, 380, 508, 762, 1016, 2034, 4068 };

	void Apu::master_control_write(uint8 data) {
		master_control.byte = data & 0x1f;
		if (!master_control.pulse1_enable)
			length_count[0] = 0;
		if (!master_control.pulse2_enable)
			length_count[1] = 0;
		if (!master_control.triangle_enable)
			length_count[2] = 0;
	}

	void Apu::pulse_coarse_write(int unit, uint8 data) {
		pulse_coarse[unit].byte = data;
		if ((unit == 0 && master_control.pulse1_enable) || (unit == 1 && master_control.pulse2_enable))
			length_count[unit] = duration_table[pulse_coarse[unit].length_index];
		envelope_start_flag[unit] = true;
		pulse_phase[unit] = 0;
	}

	void Apu::triangle_coarse_write(uint8 data) {
		triangle_coarse.byte = data;
		//Not mentioned in documentation!
		if (master_control.triangle_enable)
			length_count[2] = duration_table[triangle_coarse.length_index];
		linear_reload_flag = true;
	}

	void Apu::noise_coarse_write(uint8 data) {
		noise_coarse.byte = data;

		if (master_control.noise_enable)
			length_count[3] = duration_table[noise_coarse.length_index];
		noise_start_flag = true;
	}

	void Apu::pulse_sweep_write(int unit, uint8 data) {
		pulse_sweep[unit].byte = data;
		sweep_reload_flag[unit] = true;
	}

	void Apu::frame_control_write(uint8 data) {
		frame_control.byte = data & 0xc0;
		reset_frame();
	}

	uint8 Apu::status_read() const {
		//TODO: frame interrupt....
		uint8 out = 0;
		out |= (length_count[0]) ? 1 : 0;
		out |= (length_count[1]) ? 2 : 0;
		out |= (length_count[2]) ? 4 : 0;
		out |= (length_count[3]) ? 8 : 0;

		return out;
	}

	void Apu::reset_frame() {
		frame_counter = 0;
		clock_linear_counter();
	}

	std::string get_note_name(float freq) {
		const float A4_freq = 440.;
		const float base = 4;
		const std::string prefix[12] = { "A", "Bb", "B", "C", "C#", "D", "Eb", "E", "F", "F#", "G", "Ab" };
		float n = round(12 * log2f(freq / A4_freq));
		int octave = static_cast<int>(floor(base + (n / 12)));
		int note = static_cast<int>(n - (octave - base) * 12);
		return prefix[note] + std::to_string(octave);
	}

	void Apu::clock_envelope() {
		for (int i = 0; i < 2; i++) {
			if (envelope_start_flag[i]) {
				envelope_start_flag[i] = false;
				envelope[i] = 15;
				envelope_divider[i] = pulse_ctrl[i].n;
				//printf("KEY ON P%d [%s] [f=%g]\n", i, get_note_name(get_pulse_frequency(i)).c_str(), get_pulse_frequency(i));
			}
			else {
				if (envelope_divider[i])
					envelope_divider[i]--;
				else if (envelope[i]) {
					envelope[i]--;
					envelope_divider[i] = pulse_ctrl[i].n;
				}
				else if (pulse_ctrl[i].length_disable) {
					envelope[i] = 15;
					envelope_divider[i] = pulse_ctrl[i].n;
				}
			}
		}
		if (noise_start_flag) {
			noise_start_flag = false;
			noise_envelope = 15;
			noise_envelope_divider = noise_ctrl.n;
			//printf("KEY NOISE L:%d P:%d D:%d %s\n", duration_table[noise_coarse.length_index], noise_period_table[noise_period.period_index], noise_ctrl.n, noise_ctrl.length_disable ? "FOREVER" : "FINITE");
		}
		else {
			if (noise_envelope_divider)
				noise_envelope_divider--;
			else if (noise_envelope) {
				noise_envelope--;
				noise_envelope_divider = noise_ctrl.n;
			}
			else if (noise_ctrl.length_disable) {
				noise_envelope = 15;
				noise_envelope_divider = noise_ctrl.n;
			}
		}
	}

	unsigned short Apu::get_adjust_period(int unit) const {
		int new_timer = pulse_period_low[unit] | (pulse_coarse[unit].period_high << 8);
		int adjustment = new_timer >> (pulse_sweep[unit].shift);
		if (!pulse_sweep[unit].direction)
			new_timer += adjustment;
		else if (unit == 0) {
			new_timer += ~adjustment;
		}
		else {
			new_timer -= adjustment;
		}
		return new_timer;
	}

	void Apu::update_period(int unit) {
		auto new_timer = get_adjust_period(unit);
		pulse_period_low[unit] = new_timer & 0xff;
		pulse_coarse[unit].period_high = (new_timer >> 8) & 0x07;
	}

	void Apu::clock_sweep() {
		for (int i = 0; i < 2; i++) {
			if (sweep_reload_flag[i]) {
				if (!sweep_divider[i] && pulse_sweep[i].enable && pulse_sweep[i].shift)
					update_period(i);
				sweep_divider[i] = pulse_sweep[i].period;
				sweep_reload_flag[i] = false;
			}
			else {
				if (sweep_divider[i])
					sweep_divider[i]--;
				else if (pulse_sweep[i].enable) {
					sweep_divider[i] = pulse_sweep[i].period;
					if (pulse_sweep[i].shift)
						update_period(i);
				}
			}
		}

	}

	void Apu::clock_length() {
		for (size_t i = 0; i < 2; i++) {
			if (length_count[i] && !pulse_ctrl[i].length_disable)
				length_count[i]--;
		}
		//Triangle flag
		if (length_count[2] && !triangle_halt_flag)
			length_count[2]--;
		//Noise length
		if (length_count[3] && !noise_ctrl.length_disable)
			length_count[3]--;

	}

	void Apu::clock_linear_counter() {
		if (linear_reload_flag) {
			linear_counter = triangle_ctrl.reload;
		}
		else if (linear_counter)
			linear_counter--;
		if (!triangle_ctrl.control_flag)
			linear_reload_flag = false;
	}

	uint8 Apu::get_envelope(int unit) const {
		if (pulse_ctrl[unit].envelope_disable)
			return pulse_ctrl[unit].n;
		else
			return envelope[unit];
	}

	uint8 Apu::get_noise_envelope() const {
		if (noise_ctrl.envelope_disable)
			return noise_ctrl.n;
		else
			return noise_envelope;
	}

	float Apu::get_pulse_frequency(int unit) const {
		int pulse_timer = (pulse_period_low[unit] | (pulse_coarse[unit].period_high << 8)) + 1;
		float pulse_freq = (float)System::ntsc_frequency / (16 * (pulse_timer));
		return pulse_freq;
	}

	float Apu::get_triangle_frequency() const {
		int timer = (triangle_period_low | (triangle_coarse.period_high << 8)) + 1;
		return (float)System::ntsc_frequency / (32 * timer);
	}

	void Apu::pulse_generate(int unit, int samples) {
		bool channel_enabled = (unit) ? master_control.pulse2_enable : master_control.pulse1_enable;
		channel_enabled &= length_count[unit] && (get_adjust_period(unit) <= 0x7ff);

		uint8 envelope = get_envelope(unit);
		auto pulse_freq = get_pulse_frequency(unit);

		if (channel_enabled) {
			int duty = duty_table[pulse_ctrl[unit].duty];
			for (int i = 0; i < samples; i++) {
				int phase = static_cast<int>((8 * pulse_phase[unit])) & 0x7;
				int cur_state = duty & (0x1 << phase);
				pulse_out[unit][sample_counter + i] = cur_state ? envelope : 0;
				pulse_phase[unit] += pulse_freq / 48000;
			}
		}
		else {
			for (int i = 0; i < samples; i++) {
				pulse_out[unit][sample_counter + i] = envelope;
			}
			//pulse_phase[unit] += pulse_freq * samples / 48000;
		}

		if (pulse_phase[unit] > 1.0)
			pulse_phase[unit] = pulse_phase[unit] - floor(pulse_phase[unit]);
	}

	uint8 Apu::get_triangle_level() const {
		return triangle_table[static_cast<int>(triangle_phase * 32) & 0x1f];
	}

	void Apu::triangle_generate(int samples) {
		bool channel_enabled = master_control.triangle_enable;
		channel_enabled &= length_count[2] && linear_counter;
		auto triangle_freq = get_triangle_frequency();

		if (channel_enabled) {
			for (int i = 0; i < samples; i++) {
				triangle_out[sample_counter + i] = get_triangle_level();
				triangle_phase += triangle_freq / 48000;
			}
		}
		else {
			for (int i = 0; i < samples; i++) {
				triangle_out[sample_counter + i] = get_triangle_level();
			}
		}
		if (triangle_phase > 1.0)
			triangle_phase = triangle_phase - floor(triangle_phase);
	}

	void Apu::next_noise_bit() {
		bool feedback = (noise_period.mode ? (lsfr_reg >> 6) : (lsfr_reg >> 1)) & 0x1;
		feedback ^= (lsfr_reg & 0x1);
		lsfr_reg = (lsfr_reg >> 1) | ((feedback) ? (1 << 14) : 0);
	}

	uint8 Apu::get_current_noise_bit() const {
		return lsfr_reg & 0x1;
	}

	float Apu::integrate_noise() {
		float transition_period = noise_period_table[noise_period.period_index] / (float)System::ntsc_frequency;
		const float integration_period = 1. / 48000;
		float sample_required = integration_period / transition_period;
		float level = 0;

		//Integrate remainder of sample if small enough to avoid transition
		if (sample_required < (1 - noise_fractional_sample)) {
			noise_fractional_sample += sample_required;
			return get_current_noise_bit();
		}
		else if (sample_required == (1 - noise_fractional_sample)) {
			noise_fractional_sample = 0;
			level = get_current_noise_bit();
			next_noise_bit();
			return level;
		}
		level = get_current_noise_bit() * (1 - noise_fractional_sample) / sample_required;
		next_noise_bit();
		float remainder = sample_required - (1 - noise_fractional_sample);
		for (int i = 0; i < (int)floor(remainder); i++) {
			level += get_current_noise_bit() / sample_required;
			next_noise_bit();
		}
		noise_fractional_sample = remainder - floor(remainder);
		level += noise_fractional_sample * get_current_noise_bit() / sample_required;

		return level;
	}

	void Apu::noise_generate(int samples) {
		bool channel_enabled = master_control.noise_enable && length_count[3];

		if (channel_enabled) {
			for (int i = 0; i < samples; i++)
				noise_out[sample_counter + i] = integrate_noise() * get_noise_envelope();
		}
		else {
			for (int i = 0; i < samples; i++) {
				integrate_noise();
				noise_out[sample_counter + i] = get_noise_envelope();
			}
		}
	}

	int Apu::trigger_irq() {
		//TODO: Proper IRQ handling with OR of multiple lines, IRQ ignored if masked
		//printf("IRQ TRIGGERED\n");
		if (!(system.cpu.f & Cpu::FLAG_I))
			return system.cpu.irq();
		return 0;
	}

	void Apu::generate_frame(int samples) {
		pulse_generate(0, samples);
		pulse_generate(1, samples);
		triangle_generate(samples);
		noise_generate(samples);
		sample_counter += samples;
	}

	int Apu::run_scanline(int line) {
		int extra_cycles = 0;
		//262 lines, 800 samples.
		scanline_counter++;
		if (scanline_counter >= 65) {
			generate_frame(200);
			frame_counter++;
			scanline_counter -= 65;
			if (frame_control.five_frame) {
				if ((frame_counter % 5) != 4) {
					clock_envelope();
					clock_linear_counter();
				}
				if ((frame_counter % 5 == 0) || (frame_counter % 5 == 2)) {
					clock_length();
					clock_sweep();
				}
			}
			else {
				clock_envelope();
				clock_linear_counter();
				if (frame_counter & 0x1) {
					clock_length();
					clock_sweep();
				}
				if ((frame_counter & 0x3) == 3 && !frame_control.disable_irq)
					extra_cycles = trigger_irq();
			}
		}
		frame_counter = frame_counter % 20;
		return extra_cycles;
	}

	void Apu::new_frame() {
		scanline_counter = 0;
		//frame_counter = 0;
		sample_counter = 0;
		memset(buffer, 0, sizeof(buffer));
		memset(pulse_out[0], 0, sizeof(pulse_out[0]));
		memset(pulse_out[1], 0, sizeof(pulse_out[1]));
		memset(triangle_out, 0, sizeof(triangle_out));
		memset(noise_out, 0, sizeof(noise_out));
	}

	void Apu::end_frame() {
		//0x3fff & (envelope << 10)
		for (int i = 0; i < 800; i++) {
			float pulse_level;
			if (!pulse_out[0][i] && !pulse_out[1][i])
				pulse_level = 0;
			else
				pulse_level = 95.88f / (100 + (float)8128 / (pulse_out[0][i] + pulse_out[1][i]));

			float other_level;
			if (!triangle_out[i] && noise_out[i] == 0)
				other_level = 0;
			else
				other_level = 159.79f / (100 + (1 / ((float)triangle_out[i] / 8277 + noise_out[i] / 12241)));

			float level = pulse_level + other_level;
			buffer[i << 1] = static_cast<unsigned short>(0x3fff * level);
			buffer[(i << 1) | 1] = buffer[i << 1];
		}
	}
}