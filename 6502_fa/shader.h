#ifndef SHADER_H
#define SHADER_H
#include <vector>
#include <string>

#define GLSL(src) "#version 130\n" #src

struct Shader {
	unsigned int program;
	std::string log;

	Shader();
	~Shader();

	Shader& operator= (const Shader&) = delete;
	Shader(const Shader&) = delete;

	bool add_vertex(char *filename);
	bool add_fragment(char *filename);
	bool add_geometry(char *filename);
	bool add_shader(const char *src, unsigned int type);
	bool add_shader_file(const char *filename, unsigned int type);
	bool link();

	void use();
	void unuse();

	GLuint operator() ();

private:
	std::vector<unsigned int> vertex;
	std::vector<unsigned int> fragment;
	std::vector<unsigned int> geometry;
	
	std::string cur_file_content;
	bool readfile(const char *filename);
	
	void append_shader_log(unsigned int shader);
	void append_program_log();
};

#endif