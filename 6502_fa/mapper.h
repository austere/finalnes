#pragma once
#ifndef MAPPER_H
#define MAPPER_H
#include <cstdint>

#define DECLARE_MAPPER(M)	\
			Cartridge& cartridge; \
			M(Cartridge&); \
			virtual void reset(); \
			virtual uint8_t peek_prg(uint16_t address) const; \
			virtual uint8_t read_prg(uint16_t address); \
			virtual void write_prg(uint16_t address, uint8_t data); \
			virtual uint8_t read_chr(uint16_t address); \
			virtual void write_chr(uint16_t address, uint8_t data);\
			virtual uint8_t read_prgram(uint16_t address); \
			virtual uint8_t peek_prgram(uint16_t address); \
			virtual void write_prgram(uint16_t address, uint8_t data);

namespace Nes {
	struct Cartridge;
	struct Mapper {
		//Mapper(const Mapper& mapper) = delete;
		//Mapper& operator= (const Mapper& mapper) = delete;

		virtual void reset() = 0;
		virtual uint8_t peek_prg(uint16_t address) const = 0;
		virtual uint8_t read_prg(uint16_t address) = 0;
		virtual void write_prg(uint16_t address, uint8_t data) = 0;
		virtual uint8_t read_chr(uint16_t address) = 0;
		virtual void write_chr(uint16_t address, uint8_t data) = 0;
		virtual uint8_t read_prgram(uint16_t address) = 0;
		virtual uint8_t peek_prgram(uint16_t address) = 0;
		virtual void write_prgram(uint16_t address, uint8_t data) = 0;
		virtual bool hsync(int line);
	};

	Mapper* make_mapper(Cartridge& cartridge, uint8_t mapper_id);

	struct Mapper0 : Mapper {
		DECLARE_MAPPER(Mapper0)
		uint8_t* prg_bank[2];
		uint8_t *chr_bank;
		bool chr_writable;
	};

	struct Mapper1 : Mapper {
		DECLARE_MAPPER(Mapper1)
		uint8_t* prg_bank[2];
		uint8_t* chr_bank[2];
		int count;
		uint8_t shift_register : 5;
		bool uses_chr_ram;

		union {
			struct {
				uint8_t mirror_control : 2;
				uint8_t slot_select : 1;
				uint8_t prg_size : 1;
				uint8_t chr_mode : 1;
			};
			uint8_t register_0 : 5;
		};

		uint8_t chr_register0 : 5;
		uint8_t chr_register1 : 5;
		union {
			struct {
				uint8_t prg_register : 4;
				uint8_t wram_disable : 1;
			};
			uint8_t register_3 : 5;
		};

		void reset_shift_register();
		void update();
	};

	struct Mapper2 : Mapper0 {
		Mapper2(Cartridge&);
		int cur_bank;
		void switch_bank(int);
		virtual void reset();
		virtual void write_prg(uint16_t address, uint8_t data);
		virtual void write_chr(uint16_t address, uint8_t data);
	};

	struct Mapper3 : Mapper0 {
		Mapper3(Cartridge&);
		int cur_bank;
		virtual void reset();
		virtual void write_prg(uint16_t address, uint8_t data);
		void update_bank();
	};

	struct Mapper4 : Mapper {
		DECLARE_MAPPER(Mapper4)
		uint8_t* prg_bank[4];
		uint8_t* chr_bank[8];
		union {
			struct {
				uint8_t chr_selected[6];
				uint8_t prg_selected[2];
			};
			uint8_t bank_register[8];
		};

		union {
			struct {
				uint8_t bank_selected : 3;
				uint8_t dummy : 3;
				uint8_t prg_mode : 1;
				uint8_t chr_invert : 1;
			};
			uint8_t bank_select_register;
		};

		bool horizontal_mirroring;
		bool chip_enabled;
		bool write_protect;
		uint8_t irq_reload;
		uint8_t irq_counter;
		bool irq_enabled;
		void update();
		bool hsync(int line);
	};

	//For Holy Diver only, fuck Cosmo Carrier lol :P
	struct Mapper78 : Mapper0 {
		Mapper78(Cartridge&);
		int cur_prg_bank;
		int cur_chr_bank;
		bool mirror_mode;
		void update();
		virtual void reset();
		virtual void write_prg(uint16_t address, uint8_t data);
	};
}
#endif
