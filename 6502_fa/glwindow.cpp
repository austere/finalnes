#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE

#include <cstdio>
#include <cstdlib>
#include "glwindow.h"

//required for Console
#include <ios>
#include <iostream>
#include <fcntl.h>

using namespace std;

char* GLWindow::classname = "GLWindow";
bool GLWindow::class_registered = false;
const int GLWindow::default_height = 600;
const int GLWindow::default_width = 600;

int SetDCPixelFormat(HDC hDC) {
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(pfd));

	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	pfd.iLayerType = PFD_MAIN_PLANE;

	int nPixelFormat = ChoosePixelFormat(hDC, &pfd);
	
	if (nPixelFormat)
		SetPixelFormat(hDC, nPixelFormat, &pfd);

	return nPixelFormat;
}

static void init_gl_system() {
	static bool inited = false;
	if (inited)
		return;
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		printf("ERROR: %s\n", glewGetErrorString(err));
	}
	else inited = true;
}

GLWindow::GLWindow(HINSTANCE instance, LPSTR title, int width, int height) 
	: instance(instance), 
	title(title), 
	width(width), height(height), 
	render_function(do_nothing),
	keydown_function(NULL),
	keyup_function(NULL),
	mousemove_function(NULL),
	leftdown_function(NULL),
	leftup_function(NULL),
	proc_function(default_extra_proc),
	quit_function(NULL),
	activated(true),
	confined(false)
{ 

}

GLWindow::GLWindow() : activated(false) {}

void GLWindow::do_nothing() {}

void GLWindow::destroy() {
	wglMakeCurrent(device_context, NULL);
	wglDeleteContext(render_context);
}

bool GLWindow::default_extra_proc(UINT message, WPARAM wParam, LPARAM lParam) {
	return false;
}

LRESULT CALLBACK GLWindow::procedure(UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		case WM_DESTROY:
			//Destroy the window
			destroy();
			//PostQuitMessage(0);
			break;
		case WM_PAINT:
			//Repaint the window
			render_function();
			SwapBuffers(device_context);
			ValidateRect(handle, NULL);
			break;
		case WM_KEYDOWN:
			if (keydown_function)
				keydown_function(wParam);
			break;
		case WM_KEYUP:
			if (keyup_function)
				keyup_function(wParam);
			break;
		case WM_LBUTTONDOWN:
			if (leftdown_function)
				leftdown_function(wParam);
			break;
		case WM_LBUTTONUP:
			if (leftup_function)
				leftup_function(wParam);
			break;
		case WM_MOUSEMOVE:
			if (mousemove_function)
				mousemove_function(wParam, lParam);
			break;
		//TODO: Proper shutdown
		case WM_CLOSE:
			if (quit_function)
				quit_function();
			else
				exit(-1);
			break;
		default:
			if (!proc_function(message, wParam, lParam))
				return DefWindowProc(handle, message, wParam, lParam);
	}

	return 0;
}

LRESULT CALLBACK GLWindow::procedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	GLWindow* pthis = (GLWindow*)GetWindowLongPtr(hWnd, GWL_USERDATA);
	
	if (pthis != NULL)
		return pthis->procedure(message, wParam, lParam);
	else
		return DefWindowProc(hWnd, message, wParam, lParam);
}

void GLWindow::create_wnd_class() {
	wnd_class.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wnd_class.lpfnWndProc = procedure;
	wnd_class.cbSize = sizeof(WNDCLASSEX);
	wnd_class.cbClsExtra = 0;
	//4 additional bytes for pointer to window object.
	wnd_class.cbWndExtra = 4;
	wnd_class.hInstance = instance;
	wnd_class.hIcon = NULL;
	wnd_class.hIconSm = NULL;
	wnd_class.hCursor = LoadCursor(NULL, IDC_CROSS);
	wnd_class.lpszMenuName = NULL;
	wnd_class.lpszClassName = "OpenGLWindow";
	wnd_class.hbrBackground = NULL;

	if(RegisterClassEx(&wnd_class) == 0) {
		MessageBox(NULL, "Failed to Register window class", "Registeration failure", MB_ICONEXCLAMATION | MB_OK);
		exit(-1);
	}

	class_registered = true;
}

bool GLWindow::create_window(void (*init_function)(void)) {
	DWORD dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	DWORD dwStyle = WS_BORDER | WS_CAPTION | WS_SYSMENU | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	//Windows have borders and these change in dimensions depending on the version of windows
	//This calculates the required dimensions to be passed on to CreateWindowEx!
	RECT rect = {0, 0, width, height};
	AdjustWindowRectEx(&rect, dwStyle, false, dwExStyle);
	int window_width = rect.right - rect.left;
	int window_height = rect.bottom - rect.top;

	handle = CreateWindowEx(dwExStyle, "OpenGLWindow", title, 
		dwStyle, CW_USEDEFAULT, CW_USEDEFAULT, 
		window_width, window_height, NULL, NULL, instance, NULL);

	//passes this into the window reserved byte area.
	SetWindowLongPtr(handle, GWL_USERDATA, (LONG_PTR)this);

	//create GL rendering context.
	device_context = GetDC(handle);
	auto pixel_format = SetDCPixelFormat(device_context);
	render_context = wglCreateContext(device_context);
	wglMakeCurrent(device_context, render_context);

	init_function();
	init_gl_system();
	
	if (wglewIsSupported("WGL_ARB_create_context") != 1) {
		printf("Failed to create an OpenGL 3.1 context\n");
		return false;
	}
	
	int attribs[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 1,
		WGL_CONTEXT_FLAGS_ARB, 0,
		0
	};

	/*const int pixel_attribs[] =
	{
		WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
		WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
		WGL_COLOR_BITS_ARB, 32,
		WGL_DEPTH_BITS_ARB, 24,
		WGL_STENCIL_BITS_ARB, 8,
		0 // End of attributes list
	};

	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(pfd));

	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	pfd.iLayerType = PFD_MAIN_PLANE;

	int nPixelFormat = ChoosePixelFormat(device_context, &pfd);

	int new_pixel_format;
	unsigned int num_formats;
	wglChoosePixelFormatARB(device_context, pixel_attribs, nullptr, 1, &new_pixel_format, &num_formats);
	if (!SetPixelFormat(device_context, new_pixel_format, &pfd)) {
		printf("Error: Couldn't set wgl pixel format.\n");
		return 0;
	}*/

	auto new_context = wglCreateContextAttribsARB(device_context, 0, attribs);
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(render_context);
	wglMakeCurrent(device_context, new_context);
	render_context = new_context;

	//TODO: Setup buffer as RGBA rather than ARGB
	//wglChoosePixelFormatARB
	printf("OpenGL 3.1 context created\n");
	
	if (!handle) {
		MessageBox(NULL, "Failed to Create window handle", "Handle creation failure", MB_ICONEXCLAMATION | MB_OK);
		return false;
	}
	return true;
}

void GLWindow::init(void(*init_function)(void)) {
	if (!class_registered)
		create_wnd_class();
	create_window(init_function);
}

void GLWindow::show(int nCmdShow = SW_SHOWNORMAL) {
	ShowWindow(handle, nCmdShow);
}

void GLWindow::update() {
	UpdateWindow(handle);
}

void GLWindow::confine_mouse() {
	RECT bounded_area;
	GetClientRect(handle, &bounded_area);
	POINT& topleft = *(POINT*)&(bounded_area.left);
	POINT& bottomright = *(POINT*)&(bounded_area.right);
	ClientToScreen(handle, &topleft);
	ClientToScreen(handle, &bottomright);
	ClipCursor(&bounded_area);
}

void GLWindow::release_mouse() {
	ClipCursor(NULL);
}

POINT GLWindow::get_mouse() {
	POINT out;
	GetCursorPos(&out);
	ScreenToClient(handle, &out); 
	return out;
}

void CreateConsole(const unsigned int max_lines) {
	AllocConsole();

	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	HANDLE stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(stdHandle, &coninfo);
	coninfo.dwSize.Y = max_lines;
	SetConsoleScreenBufferSize(stdHandle, coninfo.dwSize);

	freopen("conout$", "w", stdout);
	setvbuf(stdout, NULL, _IONBF, 0);	
	std::ios::sync_with_stdio();
}

