#define _USE_MATH_DEFINES

#include "audio.h"
#include <cstring>
#include <cmath>
#include <vector>

using namespace std;

namespace Audio {
	//DirectSound interface
	IDirectSound8* dsinterface;
	DSCAPS dscaps;

	//Buffer interface
	//LPDIRECTSOUNDBUFFER primary_buffer;
	LPDIRECTSOUNDBUFFER stream_buffer;

	//Buffer variables
	WAVEFORMATEX stream_format;
	DSBUFFERDESC stream_desc;
	//WAVEFORMATEX primary_format;
	//DSBUFFERDESC primary_desc;
	DWORD buffer_size;
	DWORD buffer_in;

	//Function Prototypes
	static bool create_buffers(int sample_rate, int buffer_samples);
	static void delete_buffers();
	static void show_caps();

	//State variables
	bool initialised = false;

	DWORD get_buffer_size() {
		return buffer_size;
	}

	void get_buffer_position(DWORD* current_play, DWORD* current_write) {
		stream_buffer->GetCurrentPosition(current_play, NULL);
		*current_write = buffer_in;
	}

	int init(HWND hWnd, int sample_rate, int buffer_samples) {
		//Create interface structure
		HRESULT result = DirectSoundCreate8(NULL, &dsinterface, NULL);
		if (result != DS_OK) {
			printf("Couldn't create DirectSound: %08x\n", (int)result);
			return 0;
		}

		dscaps.dwSize = sizeof(dscaps);
		result = dsinterface->GetCaps(&dscaps);
		if (result != DS_OK) {
			printf("Couldn't get DirectSound Caps: %08x\n", (int)result);
			return 0;
		}
		show_caps();

		result = dsinterface->SetCooperativeLevel(hWnd, DSSCL_PRIORITY);
		if (result != DS_OK) {
			printf("Couldn't set Cooperative Levels: %08x\n", (int)result);
			return 0;
		}

		initialised = create_buffers(sample_rate, buffer_samples);

		return initialised;
	}

	//could check if playing beforehand then clear...
	void play() {
		if (!initialised)
			return;
		stream_buffer->SetCurrentPosition(0);
		buffer_in = buffer_size / 2;
		stream_buffer->Play(0, 0, DSBPLAY_LOOPING);
	}

	void stop() {
		if (!initialised)
			return;
		stream_buffer->Stop();
	}

	VOID CALLBACK callback(PVOID lpParam, BOOLEAN TimerOrWaitFired) {
		DWORD current_play, current_write;
		stream_buffer->GetCurrentPosition(&current_play, &current_write);

		//printf("CP:%d CW:%d\n", current_play, current_write);
	}

	static void copy_to_stream(const void* data, int chunk_size) {
		DWORD len1, len2;
		void *ptr1, *ptr2;
		HRESULT result = stream_buffer->Lock(buffer_in, chunk_size, &ptr1, &len1, &ptr2, &len2, 0);
		if (result != DS_OK) {
			printf("copy_to_stream(): Lock failed.\n");
			return;
		}

		buffer_in = (buffer_in + chunk_size) % buffer_size;

		memcpy(ptr1, data, len1);
		if (ptr2) {
			const char* next = (const char*)data + len1;
			memcpy(ptr2, next, len2);
		}
#ifdef SOUNDDEBUG
		printf("Copied %d bytes.\n", chunk_size);
#endif
		stream_buffer->Unlock(ptr1, len1, ptr2, len2);
	}

	//NOTE: very important, samples should be number of samples per single channel in buffer
	//So if the buffer has 2 channels in it, divide the buffer size by two.
	void update_stream(const void *data, int samples) {
		static bool audio_error = false;
		if (!initialised) {
			if (!audio_error) {
				printf("Audio system not initialised!\n");
				audio_error = true;
			}
			return;
		}
		int chunk_size = samples * stream_format.nBlockAlign;

		DWORD current_play, current_write;
		HRESULT result = stream_buffer->GetCurrentPosition(&current_play, &current_write);
		DWORD original_write = current_write;
		if (result != DS_OK) {
			printf("Can't get position!\n");
			return;
		}

		if (current_write < current_play)
			current_write += buffer_size;
		unsigned int data_in = buffer_in;
		if (data_in < current_write)
			data_in += buffer_size;

		for (; data_in < current_write;) {
			data_in += chunk_size;
			printf("update_stream() Underflow. [CP:%d CW:%d DI:%d OW:%d OI:%d]\n", current_play, current_write, data_in, original_write, buffer_in);
		}

		if (data_in + chunk_size > current_play + buffer_size) {
			printf("update_stream() Overflow. [CP:%d CW:%d DI:%d OW:%d OI:%d]\n", current_play, current_write, data_in, original_write, buffer_in);
			return;
		}

		//wrap back
		buffer_in = data_in % buffer_size;
#ifdef SOUNDDEBUG
		printf("COPY[CP:%d CW:%d DI:%d OW:%d OI:%d]\n", current_play, current_write, data_in, original_write, buffer_in);
#endif
		copy_to_stream((const void*)data, chunk_size);

	}

	static bool create_buffers(int sample_rate, int buffer_samples) {
		HRESULT result;

		//Wave format
		memset((void*)&stream_format, 0, sizeof(WAVEFORMATEX));
		stream_format.wFormatTag = WAVE_FORMAT_PCM;
		stream_format.nChannels = 2;
		stream_format.nSamplesPerSec = sample_rate;
		stream_format.wBitsPerSample = 16;
		stream_format.nBlockAlign = stream_format.nChannels * stream_format.wBitsPerSample / 8;
		stream_format.nAvgBytesPerSec = stream_format.nBlockAlign * stream_format.nSamplesPerSec;

		//50ms latency.
		/*
		int latency = 50;
		buffer_size = stream_format.nAvgBytesPerSec * latency / 1000;
		buffer_size = (buffer_size >> 10) << 10;
		if (buffer_size < 1024)
		buffer_size = 1024;*/

		//2 channels at 16-bits
		buffer_size = buffer_samples * stream_format.nBlockAlign;
		//start streaming into the middle
		buffer_in = 0;

		printf("Audio buffer size: %d bytes\n", buffer_size);

		memset((void*)&stream_desc, 0, sizeof(stream_desc));
		stream_desc.dwFlags = DSBCAPS_CTRLVOLUME | DSBCAPS_GLOBALFOCUS | DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_CTRLPOSITIONNOTIFY;
		stream_desc.dwBufferBytes = buffer_size;
		stream_desc.dwSize = sizeof(stream_desc);
		stream_desc.lpwfxFormat = &stream_format;

		result = dsinterface->CreateSoundBuffer(&stream_desc, &stream_buffer, NULL);
		if (result != DS_OK) {
			printf("Error creating secondary buffer: %08x.\n", result);
			return false;
		}

		void* buffer;
		DWORD locked;
		result = stream_buffer->Lock(0, buffer_size, &buffer, &locked, NULL, NULL, 0);
		if (result != DS_OK) {
			printf("Error locking secondary buffer: %08x.\n", result);
			return false;
		}

		memset(buffer, 0, locked);
		stream_buffer->Unlock(buffer, locked, NULL, 0);

		return true;
	}

	bool set_notifications(int num, uint32_t* positions, HANDLE* events) {
		if (!initialised)
			return false;

		//Notify interface
		LPDIRECTSOUNDNOTIFY8 dsound_notify;
		auto result = stream_buffer->QueryInterface(IID_IDirectSoundNotify8, (LPVOID*)&dsound_notify);
		if (result != DS_OK) {
			printf("Could not obtain notify interface: %08x\n", (int)result);
			return false;
		}
		std::vector<DSBPOSITIONNOTIFY> position_notification(num);
		int i = 0;
		for (auto& p : position_notification) {
			p.dwOffset = positions[i];
			p.hEventNotify = events[i++];
		}
		//one notification point
		result = dsound_notify->SetNotificationPositions(num, &position_notification[0]);
		dsound_notify->Release();
		if (result == DS_OK) {
			return true;
		}
		return false;
	}

	static void delete_buffers() {
		if (stream_buffer) {
			stream_buffer->Stop();
			stream_buffer->Release();
			stream_buffer = NULL;
		}
	}

	static void delete_interface() {
		if (dsinterface) {
			dsinterface->Release();
			dsinterface = NULL;
		}
	}

	static void show_caps() {
		DWORD& x = dscaps.dwFlags;

		printf("DirectSound driver capabilities:\n");
		printf("--------------------------------\n");

		if (x & DSBCAPS_CTRL3D)
			printf("Buffer has 3D control capability.\n");
		if (x & DSBCAPS_CTRLFREQUENCY)
			printf("The buffer has frequency control capability.\n");
		if (x & DSBCAPS_CTRLFX)
			printf("The buffer supports effects processing.\n");
		if (x & DSBCAPS_CTRLPAN)
			printf("The buffer has pan control capability.\n");
		if (x & DSBCAPS_CTRLVOLUME)
			printf("The buffer has volume control capability.\n");
		if (x & DSBCAPS_CTRLPOSITIONNOTIFY)
			printf("The buffer has position notification capability.\n");
		if (x & DSBCAPS_GETCURRENTPOSITION2)
			printf("The buffer uses the new behavior of the play cursor.\n");
		if (x & DSBCAPS_GLOBALFOCUS)
			printf("The buffer is a global sound buffer.\n");
	}
}