//Bit Blit Rasteriser
//(bbr)
//by abionnnn
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include "rasteriser.h"

//#include "sg.h"

using namespace std;

namespace bbr {
	const unsigned int bm_span_all[33] = {
		0x0,0x1,0x3,0x7,0xf,0x1f,0x3f,0x7f,0xff,0x1ff,0x3ff,
		0x7ff,0xfff,0x1fff,0x3fff,0x7fff,0xffff,0x1ffff,0x3ffff,
		0x7ffff,0xfffff,0x1fffff,0x3fffff,0x7fffff,0xffffff,
		0x1ffffff,0x3ffffff,0x7ffffff,0xfffffff,0x1fffffff,
		0x3fffffff,0x7fffffff,0xffffffff
	};
	const unsigned int* bm_span = bm_span_all + 1;

	Triangle::Triangle(float x1, float y1, 
             float x2, float y2,
             float x3, float y3) : 
        x1(x1), y1(y1), x2(x2), y2(y2), x3(x3), y3(y3) {}

    void Triangle::sort() {
        if (vertex_lt(x2, y2, x1, y1)) {
            swap(x1, x2);
            swap(y1, y2);
        }
        if (vertex_lt(x3, y3, x1, y1)) {
            swap(x1, x3);
            swap(y1, y3);
        }
        if (vertex_lt(x3, y3, x2, y2)) {
            swap(x2, x3);
            swap(y2, y3);
        }
    }

	void Triangle::scale(sgVec2 scale) {
		x1 *= scale[0];
		x2 *= scale[0];
		x3 *= scale[0];
		y1 *= scale[1];
		y2 *= scale[1];
		y3 *= scale[1];
	}

	void Triangle::offset(sgVec2 offset) {
		x1 += offset[0];
		x2 += offset[0];
		x3 += offset[0];
		y1 += offset[1];
		y2 += offset[1];
		y3 += offset[1];
	}

    inline bool Triangle::vertex_lt(float x1, float y1, float x2, float y2) {
        if (y1 < y2)
            return true;
        else if (y1 == y2)
            return x1 <= x2;
        return false;
    }

    void draw_Triangle(Bitmap32& bm, Triangle t, sgVec2 scale, sgVec2 offset) {
		t.scale(scale);
		t.offset(offset);
        t.sort();

		if (t.y1 == t.y2)
			draw_bottomTriangle(bm, t);
		else if (t.y2 == t.y3)
			draw_topTriangle(bm, t);
		else {
			//TODO: return if y3=y1 (lol, horizontal line :P)
			sgFloat leftx = t.x1 + (t.y2 - t.y1) * (t.x3 - t.x1)/(t.y3 - t.y1);
			sgFloat rightx = t.x2;
			
			if (leftx > rightx)
				swap(leftx, rightx);
			
			Triangle topTriangle = t;
			topTriangle.x2 = leftx;
			topTriangle.x3 = rightx;
			topTriangle.y3 = t.y2;

			Triangle bottomTriangle = t;
			bottomTriangle.x1 = leftx;
			bottomTriangle.y1 = t.y2;
			bottomTriangle.x2 = rightx;
			
			draw_topTriangle(bm, topTriangle);
			draw_bottomTriangle(bm, bottomTriangle);
		}
    }

    void draw_topTriangle(Bitmap32& bm, Triangle& t) {
		sgFloat deltay = t.y2 - t.y1;
		sgFloat dx_left = (t.x2 - t.x1) / deltay;
		sgFloat dx_right = (t.x3 - t.x1) / deltay;

		int miny = max(static_cast<int>(ceil(t.y1)), 0);
		int maxy = min(static_cast<int>(ceil(t.y2)) - 1, 31);
		sgFloat x_left = t.x1 + dx_left * (miny - t.y1);
		sgFloat x_right = t.x1 + dx_right * (miny - t.y1);

		for(int y = miny; y <= maxy; y++) {
			int clip_left = max(static_cast<int>(ceil(x_left)), 0);
			int clip_right = min(static_cast<int>(ceil(x_right))-1, 31);
			bm[y] |= bm_span[max(-1, clip_right - clip_left)] << clip_left;
			x_left += dx_left;
			x_right += dx_right;
		}
    }

    void draw_bottomTriangle(Bitmap32& bm, Triangle& t) {
		sgFloat deltay = t.y3 - t.y1;
		sgFloat dx_left = (t.x3 - t.x1) / deltay;
		sgFloat dx_right = (t.x3 - t.x2) / deltay;

		int miny = max(static_cast<int>(ceil(t.y1)), 0);
		int maxy = min(static_cast<int>(ceil(t.y3)) - 1, 31);
		sgFloat x_left = t.x1 + dx_left * (miny - t.y1);
		sgFloat x_right = t.x2 + dx_right * (miny - t.y1);

		for(int y = miny; y <= maxy; y++) {
			int clip_left = max(static_cast<int>(ceil(x_left)), 0);
			int clip_right = min(static_cast<int>(ceil(x_right))-1, 31);
			bm[y] |= bm_span[max(-1, clip_right - clip_left)] << clip_left;
			x_left += dx_left;
			x_right += dx_right;
		}
    }


}
