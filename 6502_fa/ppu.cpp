#include <memory>
#include "nes.h"

using namespace std;

namespace Nes {
	//============
	//Ppu::
	Ppu::Ppu(System& system) : system(system) {	}

	void Ppu::vblank() {
		vint = true;
		status.vblank_started = true;
	}

	void Ppu::vblank_end() {
		vint = false;
	}

	void Ppu::init() {
		memcpy(palette_memory, startup_palette, sizeof(palette_memory));
		status.byte = 0;
		ctrl.byte = 0;
		address.data = 0;
		new_address.data = 0;
		oam_address = 0;
		sprite0_hit_raised = false;
		vint = false;
	}

	unsigned char Ppu::read(unsigned short address) {
		//printf("PPU READ [$%04x]\n", address);
		switch (address & 0x2000) {
		case 0x0000:
			return system.cartridge.mapper->read_chr(address);
			break;
		case 0x2000:
			if (address >= 0x3f00) {
				if ((address & 0x3) == 0)
					return palette_memory[address & 0xf];
				else
					return palette_memory[address & 0x1f];
			}
			else {
				//TODO: 4 screen case (and 00 case)
				int table_select = (address >> 10) & 0x3;
				return table[system.cartridge.table_map[table_select]].byte[address & 0x3ff];
			}
			break;
		default:
			return 0;
		}
	}

	void Ppu::write(unsigned short address, unsigned char data) {
		//printf("PPU WRITE [$%04x] : $%02x\n", address, data);
		switch (address & 0x2000) {
		case 0x0000:
			system.cartridge.mapper->write_chr(address, data);
			break;
		case 0x2000:
			if (address >= 0x3f00) {
				if ((address & 0x3) == 0)
					palette_memory[address & 0xf] = data & 0x3f;
				else
					palette_memory[address & 0x1f] = data & 0x3f;
			}
			else {
				int table_select = (address >> 10) & 0x3;
				table[system.cartridge.table_map[table_select]].byte[address & 0x3ff] = data;
			}
			break;
		}
	}

	unsigned short Ppu::nametable_address(int x, int y) const {
		unsigned short base = 0x2000;
		if (y >= 240) {
			base = 0x2800;
			y -= 240;
		}
		if (x & 0x100)
			base |= 0x400;
		x &= 0xff;

		return base + (x >> 3) + ((y >> 3) << 5);
	}

	unsigned char Ppu::get_attribute(unsigned short address) {
		unsigned short target = address & 0x2c00;
		const char doubles[4] = { 0, 2, 4, 6 };
		target |= 0x3c0 | ((address >> 2) & 0x7) | ((address >> 4) & 0x38);
		int selector = ((address >> 1) & 0x1) | ((address >> 5) & 0x2);
		unsigned char byte = read(target);
		return (byte >> doubles[selector]) & 0x3;
	}

	inline unsigned char mirror_bits(unsigned char byte) {
		byte = ((byte & 0x0f) << 4) | (byte >> 4);
		byte = ((byte & 0x33) << 2) | ((byte & 0xcc) >> 2);
		return ((byte & 0x55) << 1) | ((byte & 0xaa) >> 1);
	}

	uint8_t Ppu::get_xscroll() const {
		return (address.x_coarse << 3) | x_fine;
	}

	uint8_t Ppu::get_yscroll() const {
		return (address.y_coarse << 3) | address.y_fine;
	}

	uint8_t Ppu::get_basetable() const {
		return address.base_table;
	}

	void Ppu::start_frame() {
		if (mask.enable_background || mask.enable_sprite) {
			//address.data &= 0x1f;
			//address.data |= new_address.data & (~0x1f);
			address.data = new_address.data;
		}
	}

	void Ppu::next_scanline() {
		if (mask.enable_background || mask.enable_sprite) {
			//increment Y
			uint8_t new_y = (get_yscroll() + 1);
			address.y_fine = new_y & 0x7;
			address.y_coarse = new_y >> 3;
			if (address.y_coarse == 30) {
				address.y_coarse = 0;
				address.base_table ^= 0x2;
			}

			//increment X			
			address.x_coarse = new_address.x_coarse;
			address.base_table &= 0x2;
			address.base_table |= new_address.base_table & 0x1;
		}
	}

	void Ppu::render_name_table(uint32_t *target) {
		const int max_x = 256 * 2;
		const int max_y = 240 * 2;
		memset(target, 0, max_x * max_y * sizeof(uint32_t));
		for (int y = 0; y < max_y; y++) {
			for (int x = 0; x < max_x; x++) {
				unsigned short address = nametable_address(x, y);
				unsigned char name = read(address);
				unsigned char palette = get_attribute(address);
				unsigned char pattern_low = read((name << 4) + (ctrl.background_table << 12) + (y & 0x7));
				unsigned char pattern_high = read((name << 4) + (ctrl.background_table << 12) + (y & 0x7) + 8);
				unsigned char low = (pattern_low << (x & 0x7)) & 0x80;
				unsigned char high = (pattern_high << (x & 0x7)) & 0x80;
				unsigned char index = (high >> 6) | (low >> 7);
				if (index)
					index |= (palette << 2);
				target[y * max_x + x] = (palette_rgb[palette_memory[index]] << 8) | 0xff;
			}
		}
		if (mask.enable_background || mask.enable_sprite) {
			for (int x = 0; x < max_x; x++) {
				//uint16_t offset = (get_basetable() & 0x02) ? 240 : 0;
				int cx = x;
				int cy = get_yscroll();
				if (get_basetable() & 0x2)
					cy += 240;
				int offset = cy * max_x + cx;
				if (offset >= (max_x * max_y)) {
					//printf("Shitty kaka :C");
					return;
				}
				uint32_t* p = target + offset;
				*p ^= 0xffffff00;
			}

			for (int y = 0; y < max_y; y++) {
				int cx = get_xscroll(); // ((get_basetable() & 0x01) ? 256 : 0);
				int cy = y;
				if (get_basetable() & 0x1)
					cx += 256;
				int offset = cy * max_x + cx;
				uint32_t* p = target + offset;
				*p ^= 0xffffff00;
			}
		}
	}

	void Ppu::render_sprite(uint8_t sprite, uint32_t* target) {
		//8x16 or 8x8 sprites
		int sprite_height = (ctrl.sprite_size) ? 16 : 8;
		unsigned char palette = oam_attributes[sprite].palette;
		for (int y = 0; y < sprite_height; y++) {
			int dy = y;
			if (oam_attributes[sprite].flip_y)
				dy = sprite_height - 1 - y;
			unsigned short address;
			if (sprite_height == 16) {
				int offset = (dy < 8) ? dy : ((dy - 8) + 16);
				address = ((oam_attributes[sprite].index & 0x1) << 12) | ((oam_attributes[sprite].index & 0xfe) << 4) + offset;
			}
			else
				address = (ctrl.base_sprite << 12) | (oam_attributes[sprite].index << 4) + dy;
			unsigned char pattern_low = read(address);
			unsigned char pattern_high = read(address + 8);
			if (oam_attributes[sprite].flip_x) {
				pattern_low = mirror_bits(pattern_low);
				pattern_high = mirror_bits(pattern_high);
			}
			for (int x = 0; x < 8; x++) {
				unsigned char low = (pattern_low << x) & 0x80;
				unsigned char high = (pattern_high << x) & 0x80;
				unsigned char index = (high >> 6) | (low >> 7);
				if (index)
					index |= (palette << 2);
				target[y * 8 + x] = (palette_rgb[palette_memory[index]] << 8) | 0xff;
			}
		}
	}

	//Need to optimise this
	void Ppu::render_line(int line) {
		//Work out sprites (TODO: pipeline properly, i.e. in the previous scan)
		unsigned char sprites[256 + 8];
		memset(sprites, 0, sizeof(sprites));

		//8x16 or 8x8 sprites
		int sprite_height = (ctrl.sprite_size) ? 16 : 8;

		for (int cur_sprite = 63; cur_sprite >= 0; cur_sprite--) {
			//if (oam_attributes[cur_sprite].y == 238 && line == 239)
			//	printf("BADINGDING!");
			int dy = (oam_attributes[cur_sprite].y + 1) - line + sprite_height - 1;
			if (dy >= sprite_height || dy < 0)
				continue;
			if (!oam_attributes[cur_sprite].flip_y)
				dy = sprite_height - 1 - dy;
			unsigned short address;
			if (sprite_height == 16) {
				int offset = (dy < 8) ? dy : ((dy - 8) + 16);
				address = ((oam_attributes[cur_sprite].index & 0x1) << 12) | ((oam_attributes[cur_sprite].index & 0xfe) << 4) + offset;
			}
			else
				address = (ctrl.base_sprite << 12) | (oam_attributes[cur_sprite].index << 4) + dy;
			unsigned char palette = oam_attributes[cur_sprite].palette;
			unsigned char pattern_low = read(address);
			unsigned char pattern_high = read(address + 8);
			if (oam_attributes[cur_sprite].flip_x) {
				pattern_low = mirror_bits(pattern_low);
				pattern_high = mirror_bits(pattern_high);
			}

			for (int j = 0; j < 8; j++) {
				unsigned char low = (pattern_low << j) & 0x80;
				unsigned char high = (pattern_high << j) & 0x80;
				unsigned char index = (high >> 6) | (low >> 7);
				if (index)
					index |= (palette << 2) | 0x10 | (oam_attributes[cur_sprite].priority << 5);
				int sprite_x = oam_attributes[cur_sprite].x + j;
				//if the existing data is in the back, use this one
				//if ((sprites[sprite_x] & 0x20) || !sprites[sprite_x])
				if (index)
					sprites[sprite_x] = index;
				//toggle bit for sprite 0 hit detection.
				if (cur_sprite == 0 && index)
					sprites[sprite_x] |= 0x40;
			}

		}

		int cur_x = get_xscroll() | ((get_basetable() & 0x1) << 8);
		int cur_y = (get_basetable() & 0x2) ? 240 : 0;
		cur_y += get_yscroll();

		unsigned char background[33 * 8];
		//unsigned short base = 0x2000 | (ctrl.get_basetable() << 10);
		//memset(background, 0, sizeof(background));

		for (int i = 0; i < 33; i++, cur_x += 8) {
			//int cur_x = i + (get_xscroll() >> 3);
			unsigned short address = nametable_address(cur_x, cur_y);
			unsigned char name = read(address);
			unsigned char palette = get_attribute(address);
			unsigned char pattern_low = read((name << 4) + (ctrl.background_table << 12) + (cur_y & 0x7));
			unsigned char pattern_high = read((name << 4) + (ctrl.background_table << 12) + (cur_y & 0x7) + 8);

			for (int j = 0; j < 8; j++) {
				unsigned char low = (pattern_low << j) & 0x80;
				unsigned char high = (pattern_high << j) & 0x80;
				unsigned char index = (high >> 6) | (low >> 7);
				if (index)
					index |= (palette << 2);
				background[(i << 3) | j] = index;
			}
		}
		//TODO: Fetch 34th tuple but don't do anything with it (for mapper)

		//TODO: enable_background, enable_sprite, etc.
		//Mix them into the output buffer using NES Palettes
		for (int x = 0; x < 256; x++) {
			int index = background[x + (get_xscroll() & 0x7)];
			if (!mask.enable_background)
				index = 0;
			int sprite_index = sprites[x];
			if (!mask.enable_sprite)
				sprite_index = 0;

			//Clipping
			if (x < 8) {
				if (!mask.enable_bck_left)
					index = 0;
				if (!mask.enable_spr_left)
					sprite_index = 0;
			}

			//Sprite 0 detection
			if ((sprite_index & 0x40) && index && !sprite0_hit_raised && (x < 255) && (line < 240)) {
				//printf("sprite 0 hit: %d\n", line);
				status.sprite0_hit = true;
				sprite0_hit_raised = true;
			}
			if (sprite_index && (!(sprite_index & 0x20) || !index))
				index = sprite_index & 0x1f;
			output[(line << 8) | x] = (palette_rgb[palette_memory[index]] << 8) | 0xff;
		}

		//Temporary hack 1
		/*if (line > oam_attributes[0].y + 8 && !sprite0_hit_raised) {
		status.sprite0_hit = true;
		sprite0_hit_raised = true;
		}*/

	}
}