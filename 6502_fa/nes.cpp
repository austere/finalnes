#define _CRT_SECURE_NO_WARNINGS
//#pragma warning( disable : 4355 )

#include "nes.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <string>

using namespace std;

namespace Nes {
	const char opcode_mnemonic[][4] = {
		"non",
		"ADC", "AND", "ASL", "BCC", "BCS", "BEQ", "BIT", "BMI", "BNE", "BPL", "BRK", "BVC", "BVS", "CLC",
		"CLD", "CLI", "CLV", "CMP", "CPX", "CPY", "DEC", "DEX", "DEY", "EOR", "INC", "INX", "INY", "JMP",
		"JSR", "LDA", "LDX", "LDY", "LSR", "NOP", "ORA", "PHA", "PHP", "PLA", "PLP", "ROL", "ROR", "RTI",
		"RTS", "SBC", "SEC", "SED", "SEI", "STA", "STX", "STY", "TAX", "TAY", "TSX", "TXA", "TXS", "TYA"
	};

	//============
	//Controller::
	// Controller (you can hook in input code externally)
	Controller::Controller() : input_function(default_get_input) { }
	void Controller::default_get_input(Controller* ) { }

	void Controller::init() {
		for(int i=0; i<8; i++)
			state_register[i] = false;
		strobed = false;
		readout = false;
		index = 0;
	}

	//TODO: Rewrite this to account for all behaviour
	void Controller::write(unsigned char data) {
		if (data & 0x1) {
			strobed = true;
			input_function(this);
		}
		else if (strobed) {
			strobed = false;
			readout = true;
			index = 0;
		}
	}

	unsigned char Controller::read() {
		if (readout && index < 8)
			return state_register[index++];
		else
			return 1;
	}

	//============
	//Cartridge:
	// Quite primitive atm
	//TODO: Combine with Mapper, i.e we need a create_cartridge() function, overload Cartridge etc.
	//perhaps call it board?
	Cartridge::Cartridge() : loaded(false) { }

	bool Cartridge::load(const char* filename) {
		if (loaded)
			unload();

		printf("Loading [%s]\n", filename);

		FILE *fp = fopen(filename, "rb");
		if (!fp)
			return false;

		unsigned char headbuffer[16];
		int numread = fread(headbuffer, 1, 16, fp);

		if (numread != 16 ||
			headbuffer[0] != 'N' || headbuffer[1] != 'E' || 
			headbuffer[2] != 'S' || headbuffer[3] != '\x1a')
			return false;
		numprg = headbuffer[4];
		numchr = headbuffer[5];

		vertical_mirroring = (headbuffer[6] & 0x1) ? true : false;
		sram_enabled = (headbuffer[6] & 0x2) ? true : false;
		trainer_present = (headbuffer[6] & 0x4) ? true : false;
		four_screen = (headbuffer[6] & 0x8) ? true : false;
		mapper_id = (headbuffer[7] & 0xf0) | (headbuffer[6] >> 4);

		//Initial settings
		//if vertical {0, 1, 0, 1}
		if (vertical_mirroring) {
			table_map[0] = table_map[2] = 0;
			table_map[1] = table_map[3] = 1;
		}
		else {
			table_map[0] = table_map[1] = 0;
			table_map[2] = table_map[3] = 1;
		}

		//TODO: check byte #7 and #10 for more information about cartridge
		//TODO: error checking: make sure data was actually read!
		prg_rom = new PRG_Rom[numprg];
		for (int i=0; i < numprg; i++)
			fread(prg_rom[i], sizeof(PRG_Rom), 1, fp);
		if (numchr) {
			chr_rom = new CHR_Rom[numchr];
			for (int i=0; i < numchr; i++)
				fread(chr_rom[i], sizeof(CHR_Rom), 1, fp);
		}
		loaded = true;

		//After loading all ROMs, link the mapper
		mapper = make_mapper(*this, mapper_id);

		fclose(fp);
		return true;
	}

	void Cartridge::unload() {
		if (!loaded)
			return;
		printf("Unloading ROM\n");
		delete [] prg_rom;
		if (numchr)
			delete [] chr_rom;
		if (mapper) {
			delete mapper;
			mapper = nullptr;
		}
		loaded = false;
	}

	uint8 Cartridge::read_prg(uint16_t address) {
		return mapper->read_prg(address);
	}

	uint8 Cartridge::peek_prg(uint16_t address) {
		return mapper->peek_prg(address);
	}

	void Cartridge::write_prg(uint16_t address, uint8_t data) {
		mapper->write_prg(address, data);
	}

	uint8 Cartridge::read_chr(uint16_t address) {
		return mapper->read_chr(address);
	}

	void Cartridge::write_chr(uint16_t address, uint8_t data) {
		mapper->write_chr(address, data);
	}

	//============
	//Memory::
	// TODO: should implement hooks into mapper
	inline unsigned char Memory::read(unsigned short address) {
		switch (address >> 13) {
			//RAM
			case 0x00: //0x0000
				return cpu_ram[address & 0x7ff];
				break;
			//I/O
			case 0x01: //0x2000
				return read_io1(address & 0x7);
				break;
			case 0x02: //0x4000
				if (address < 0x4020)
					return read_io2(address & 0x1f);
				else
					return system->cartridge.mapper->read_prg(address);
				break;
			case 0x03: //0x6000
				//SRAM (handle from mapper? breaks some cases. TODO: Turn into its own function)
				return system->cartridge.mapper->read_prgram(address);
				break;
			default:	//0x8000 - 0xffff
				return system->cartridge.mapper->read_prg(address);
				//return read_prgrom(address & 0x7fff);
				break;
		}
		return 0;
	}

	//For debugging (doesn'nt effect state)
	inline unsigned char Memory::peek(unsigned short address) {
		switch (address >> 13) {
			//RAM
			case 0x00: //0x0000
				return cpu_ram[address & 0x7ff];
				break;
			//I/O
			case 0x01: //0x2000
				return peek_io1(address & 0x7);
				break;
			case 0x02: //0x4000
				if (address < 0x4020)
					return peek_io2(address &0x1f);
				//TODO: otherwise, handle with mapper
				break;
			case 0x03: //0x6000
				//SRAM
				system->cartridge.mapper->peek_prgram(address);
				break;
			default:	//0x8000 - 0xffff
				//return peek_prgrom(address & 0x7fff);
				return system->cartridge.mapper->peek_prg(address);
				break;
		}
		return 0;
	}

	inline void Memory::write(unsigned short address, unsigned char data) {
		switch (address >> 13) {
			//RAM
			case 0x00: //0x0000
				cpu_ram[address & 0x7ff] = data;
				break;
			//I/O
			case 0x01: //0x2000
				write_io1(address & 0x7, data);
				break;
			case 0x02: //0x4000
				if (address < 0x4020)
					write_io2(address & 0x1f, data);
				//TODO: otherwise, handle with mapper
				else
					system->cartridge.mapper->write_prg(address, data);
					//printf("Mapper[$%04x] = $%0x2\n", address, data);
				break;
			case 0x03: //0x6000
				//TODO: Mapper or SRAM
				//printf("Mapper[$%04x] = $%0x2\n", address, data);
				system->cartridge.mapper->write_prgram(address, data);
				//SRAM
				break;
			default:	//0x8000 - 0xffff
				//TODO: Mapper or SRAM
				//(address & 0x7fff)
				//printf("Mapper[$%04x] = $%0x2\n", address, data);
				system->cartridge.mapper->write_prg(address, data);
				break;
		}
	}

	inline unsigned char Memory::read_io1(unsigned short address) {
		unsigned char ret = 0;
		switch(address) {
			case 0x0:	//PPUCTRL $2000
				ret = system->ppu.ctrl.byte & ~0x3;
				ret |= system->ppu.address.base_table;
				break;
			case 0x1:	//PPUMASK $2001
				return system->ppu.mask.byte;
			case 0x2:	//PPUSTATUS $2002
				ret = system->ppu.status.byte;
				system->ppu.status.vblank_started = false;
				system->ppu.vram_toggle = false;
				return ret;
			case 0x3:	//OAMADDR $2003
				break;
			case 0x4:	//OAMDATA $2004
				if (system->ppu.vint)
					return system->ppu.oam[system->ppu.oam_address];
				else
					return system->ppu.oam[(system->ppu.oam_address)++];
			case 0x5:	//PPUSCROLL $2005
				break;
			case 0x6:	//PPUADDR $2006
				break;
			case 0x7:	//PPUDATA $2007
				if (system->ppu.address.data < 0x3f00) {
					ret = system->ppu.data;
					system->ppu.data = system->ppu.read(system->ppu.address.data);
				}
				else {
					ret = system->ppu.read(system->ppu.address.data);
					system->ppu.data = system->ppu.read(system->ppu.address.data & ~0x1000);
				}

				if (system->ppu.ctrl.vram_increment)
					system->ppu.address.data += 32;
				else
					++(system->ppu.address.data);
				system->ppu.address.data &= 0x3fff;
				break;
		}
		return ret;
	}

	inline unsigned char Memory::peek_io1(unsigned short address) {
		switch(address) {
			case 0x0:	//PPUCTRL $2000
				return system->ppu.ctrl.byte;
			case 0x1:	//PPUMASK $2001
				return system->ppu.mask.byte;
			case 0x2:	//PPUSTATUS $2002
				return system->ppu.status.byte;
			case 0x3:	//OAMADDR $2003
				break;
			case 0x4:	//OAMDATA $2004
				return system->ppu.oam[system->ppu.oam_address];
			case 0x5:	//PPUSCROLL $2005
				break;
			case 0x6:	//PPUADDR $2006
				break;
			case 0x7:	//PPUDATA $2007
				return system->ppu.data;
		}
		return 0;
	}

	inline void Memory::write_io1(unsigned short address, unsigned char data) {
		switch(address) {
			case 0x0:	//PPUCTRL $2000
				//TODO: Setting mask bit when there is a NMI should trigger a NMI
				system->ppu.ctrl.byte = data;
				system->ppu.new_address.base_table = system->ppu.ctrl.base_table;
				//printf("PPUCTRL:%x\n", system->ppu.new_address.base_table);
				break;
			case 0x1:	//PPUMASK $2001
				system->ppu.mask.byte = data;
				break;
			case 0x2:	//PPUSTATUS $2002
				break;
			case 0x3:	//OAMADDR $2003
				system->ppu.oam_address = data;
				break;
			case 0x4:	//OAMDATA $2004
				//if (system->ppu.oam_address < 4) {
				//	printf("OAM[%d] = $%02x\n", system->ppu.oam_address, data);
				//}
				system->ppu.oam[(system->ppu.oam_address)++] = data;
				break;
			case 0x5:	//PPUSCROLL $2005
				//First write
				if (!system->ppu.vram_toggle) {
					system->ppu.x_fine = data;
					system->ppu.new_address.x_coarse = data >> 3;
				}
				//Second write
				else {
					system->ppu.new_address.y_fine = data;
					system->ppu.new_address.y_coarse = data >> 3;
				}
				//printf("Scroll set X:%d Y:%d\n", (system->ppu.new_address.x_coarse << 3) | system->ppu.x_fine, (system->ppu.new_address.y_coarse << 3) | system->ppu.new_address.y_fine);
				system->ppu.vram_toggle = !system->ppu.vram_toggle;
				break;
			case 0x6:	//PPUADDR $2006
				//First write
				if (!system->ppu.vram_toggle) {
					system->ppu.new_address.data &= 0xff;
					system->ppu.new_address.data |= (data << 8) & 0x3f00;
				}
				//Second write
				else {
					system->ppu.new_address.data &= 0xff00;
					system->ppu.new_address.data |= data;
					system->ppu.address = system->ppu.new_address;
				}
				system->ppu.vram_toggle = !system->ppu.vram_toggle;
				break;
			case 0x7:	//PPUDATA $2007
				system->ppu.write(system->ppu.address.data, data);
				if (system->ppu.ctrl.vram_increment)
					system->ppu.address.data += 32;
				else
					++(system->ppu.address.data);
				system->ppu.address.data &= 0x3fff;
				break;
		}
		system->ppu.status.last_written = data & 0x1f;
	}

	inline unsigned char Memory::read_io2(unsigned short address) {
		switch(address) {
			case 0x15:	//pAPU Sound/Vertical Clock Signal Register
				//TODO: add the 2 IRQ bits.
				return system->apu.status_read();
			case 0x16:	//Joypad 1
				return system->controller[0].read();
			case 0x17:	//Joypad 2
				return system->controller[1].read();
			default:
				printf("Weird read $4%03x\n", address);
				return 0;
		}
		return 0;
	}

	//TODO: Implement peek_io2 for debugging
	inline unsigned char Memory::peek_io2(unsigned short address) {
		
		return 0;
	}
	
	inline void Memory::write_io2(unsigned short address, unsigned char data) {
		switch(address) {
			case 0x00:	//pAPU Pulse 1 Control Register
				system->apu.pulse_ctrl[0].byte = data;
				break;
			case 0x01:	//pAPU Pulse 1 Ramp Control Register
				system->apu.pulse_sweep_write(0, data);
				break;
			case 0x02:	//pAPU Pulse 1 Fine Tune (FT) Register
				system->apu.pulse_period_low[0] = data;
				break;
			case 0x03:	//pAPU Pulse 1 Coarse Tune (CT) Register
				system->apu.pulse_coarse_write(0, data);
				break;
			case 0x04:	//pAPU Pulse 2 Control Register
				system->apu.pulse_ctrl[1].byte = data;
				break;
			case 0x05:	//pAPU Pulse 2 Ramp Control Register
				system->apu.pulse_sweep_write(1, data);
				break;
			case 0x06:	//pAPU Pulse 2 Fine Tune (FT) Register
				system->apu.pulse_period_low[1] = data;
				break;
			case 0x07:	//pAPU Pulse 2 Corase Tune (CT) Register
				system->apu.pulse_coarse_write(1, data);
				break;
			case 0x08:	//pAPU Triangle Control Register
				system->apu.triangle_ctrl.byte = data;
				break;
			case 0x09:	//Unused
				break;
			case 0x0a:	//pAPU Triangle Frequency Register 1
				system->apu.triangle_period_low = data;
				break;
			case 0x0b:	//pAPU Triangle Frequency Register 2
				system->apu.triangle_coarse_write(data);
				break;
			case 0x0c:	//pAPU Noise Control Register 1
				system->apu.noise_ctrl.byte = data;
				break;
			case 0x0e:	//pAPU Noise Frequency Register 1
				system->apu.noise_period.byte = data;
				break;
			case 0x0f:	//pAPU Noise Frequency Register 2
				system->apu.noise_coarse_write(data);
				break;
			case 0x10:	//pAPU Delta Modulation Control Register
				break;
			case 0x11:	//pAPU Delta Modulation D/A Register
				break;
			case 0x12:	//pAPU Delta Modulation Address Register
				break;
			case 0x13:	//pAPU Delta Modulation Data Length Register
				break;
			case 0x14:	//OAM DMA Register
				system->cpu.dma_requested = true;
				system->cpu.dma_page = data;
				break;
			case 0x15:	//pAPU Sound Key on/off / Vertical Clock Signal Register
				system->apu.master_control_write(data);
				break;
			case 0x16:	//Joypad stobe
				system->controller[0].write(data);
				system->controller[1].write(data);
				break;
			case 0x17:	//pAPU frame control
				system->apu.frame_control_write(data);
				break;
			default:
				break;			
		}
	}
	
	inline unsigned short Memory::read16(unsigned short address) {
		return read(address) | (read(address+1) <<8);
	}

	inline void Memory::write16(unsigned short address, unsigned short data) {
		write(address, data & 0xff);
		write(address + 1, data >> 8);
	}

	//============
	//Cpu::

	inline void Cpu::set_sign(unsigned char data) {f = (f & ~FLAG_N) | ((data & 0x80) ? FLAG_N : 0);}
	inline void Cpu::set_overflow(unsigned char data) {f = (f & ~FLAG_V) | (data ? FLAG_V : 0);}
	//...
	inline void Cpu::set_break(unsigned char data) {f = (f & ~FLAG_B) | (data ? FLAG_B : 0);}
	inline void Cpu::set_decimal(unsigned char data) {f = (f & ~FLAG_D) | (data ? FLAG_D : 0);}
	inline void Cpu::set_interrupt(unsigned char data) {f = (f & ~FLAG_I) | (data ? FLAG_I : 0);}
	inline void Cpu::set_carry(unsigned char data) {f = (f & ~FLAG_C) | (data ? FLAG_C : 0);}
	inline void Cpu::set_zero(unsigned char data) {f = (f & ~FLAG_Z) | (!data ? FLAG_Z : 0);}

	Cpu::Cpu(System& system) : debug(false), system(system) {
	}

	//Call after setting system
	void Cpu::init() {
		//make sure memory can find everything.
		memory.system = &system;

		memset(opcode_cycles, 1, 256);
		memset(opcode_mode, imp, 256*4);
		memset(opcode_instruction, NON, 256*4);

		for(int i=0; i < sizeof(cpu_info)/sizeof(cpu_info[0]); i++) {
			Mode mode = (Mode)cpu_info[i][0];
			unsigned char operandsize = cpu_info[i][1];
			Instruction instruction = (Instruction)cpu_info[i][2];
			unsigned char opcode = cpu_info[i][3];
			unsigned char cycles = cpu_info[i][4];
			opcode_operandsize[opcode] = operandsize;
			opcode_cycles[opcode] = cycles;
			opcode_instruction[opcode] = instruction;
			opcode_mode[opcode] = mode;
		}

		dma_requested = false;
		power();
	}

	void Cpu::power() {
		if (system.cartridge.loaded) {
			system.cartridge.mapper->reset();
			pc = memory.read16(0xfffc);
		}
		a = 0;
		x = 0;
		y = 0;
		f = 0x34;
		sp = 0xfd;
		hung = false;
		dma_requested = false;
	}

	void Cpu::reset() {
		pc = memory.read16(0xfffc);
		f |= FLAG_I;
		sp -= 3;
		hung = false;
		dma_requested = false;
	}
	
	void Cpu::push(unsigned char data) {
		memory.write(0x100 + sp, data);
		--sp;
	}

	unsigned char Cpu::pop() {
		++sp;
		return memory.read(0x100 + sp);
	}

	int Cpu::execute(int required_cycles) {
		int executed_cycles = 0;
		while(executed_cycles < required_cycles) {
			unsigned char current_opcode = memory.read(pc++);
			if (debug)
				debug_out_opcode(current_opcode);
			executed_cycles += execute_opcode(current_opcode);
			//Must do this since CPU is halted while DMA occurs
			if (dma_requested) {
				for (int i = 0, j = dma_page << 8; i < 0x100; i++, j++) {
					system.ppu.oam[(i + system.ppu.oam_address) & 0xff] = memory.read(j);
				}
				dma_requested = false;
				//Is it 512 or 513?
				executed_cycles += 512;
			}
		}

		return (executed_cycles - required_cycles);
	}

	//NMI
	int Cpu::nmi() {
		push(pc >> 8);
		push(pc & 0xff);
		push((f | 0x20) & ~0x10);
		set_interrupt(true);
		pc = memory.read16(0xfffa);
		return 7;
	}

	//IRQ
	int Cpu::irq() {
		push(pc >> 8);
		push(pc & 0xff);
		push((f | 0x20) & ~0x10);
		set_interrupt(true);
		pc = memory.read16(0xfffe);
		//Check how long an IRQ takes
		return 7;
	}

	inline uint16_t Cpu::zp_read16(unsigned char zp_address) {
		if (zp_address == 0xff)
			return memory.read(0xff) | (memory.read(0x00) << 8);
		return memory.read16(zp_address);
	}

	inline unsigned short Cpu::fetch_parameter(Mode mode) {
		switch (mode) {
		default:
			return 0;
		case zp:
		case zpind_x:
		case zpind_y:
		case preindir:
		case postindir:
			return (unsigned short)memory.read(pc);
		case abs:
		case ind_x:
		case ind_y:
		case indir:
			return memory.read16(pc);
		}
	}

	inline unsigned short Cpu::fetch_target(Mode mode) {
		switch(mode) {
			default:
				return 0;
			case abs:
				return memory.read16(pc);
			case zp:
				return (unsigned short)memory.read(pc);
			case ind_x:
				return (memory.read16(pc) + x);
			case ind_y:
				return (memory.read16(pc) + y);
			case zpind_x:
				return (memory.read(pc) + x) & 0xff;
			case zpind_y:
				return (memory.read(pc) + y) & 0xff;
			//replicate bug on NES
			case indir: 
			{
				unsigned short temp = memory.read16(pc);
				return memory.read(temp) | (memory.read(temp & 0xff00 | ((temp + 1) & 0xff)) << 8);
			}
			case preindir:
				return zp_read16((memory.read(pc) + x) & 0xff);
			case postindir:
				return zp_read16(memory.read(pc)) + y;
		}
	}

	inline unsigned short Cpu::fetch_operand(Mode mode) {
		switch(mode) {
			default:
				return 0;
			case accum:
				return a;
			case imm:
				return memory.read(pc);
			case abs:
				return memory.read(memory.read16(pc));
			case zp:
				return memory.read((unsigned short)memory.read(pc));
			//(+1 page crossed)
			case ind_x:
			{
				auto base = memory.read16(pc);
				auto address = base + x;
				if ((address ^ base) & 0x100)
					delta_cycles = 1;
				return memory.read(address);
			}
			//(+1 page crossed)
			case ind_y:
			{
				auto base = memory.read16(pc);
				auto address = base + y;
				if ((address ^ base) & 0x100)
					delta_cycles = 1;
				return memory.read(address);
			}
			case zpind_x:
				return memory.read((memory.read(pc) + x) & 0xff);
			case zpind_y:
				return memory.read((memory.read(pc) + y) & 0xff);
			case indir:
			{
				unsigned short temp = memory.read16(pc);
				return memory.read(temp) | (memory.read(temp & 0xff00 | ((temp + 1) & 0xff)) << 8);
				break;
			}
			case preindir:
				return memory.read(zp_read16((memory.read(pc) + x) & 0xff));
			//(+1 page crossed)
			case postindir:
			{
				auto base = zp_read16(memory.read(pc));
				auto address = base + y;
				if ((address ^ base) & 0x100)
					delta_cycles = 1;
				return memory.read(address);
			}
			case rel:
				return pc + 1 + (signed char)memory.read(pc);
		}
	}

	static bool debug_opened = false;
	static FILE *debug_fp;
	static const char* debug_filename = "trace.txt";

	void open_debug() {
		debug_fp = fopen(debug_filename, "w");
		debug_opened = true;
	}
	
	template<typename... Args>
	static void dprintf(char* fmt, Args... args) {
		if (!debug_opened)
			open_debug();
		fprintf(debug_fp, fmt, args...);
	}

	void Cpu::debug_out_opcode(unsigned char opcode) {
		//auto dprintf = ::printf;
		dprintf("$%04x: $%02x\t%s", pc-1, opcode, opcode_mnemonic[opcode_instruction[opcode]]);

		Mode mode = opcode_mode[opcode];
		switch(mode) {
			default:
				if (opcode_instruction[opcode] == NON)
					dprintf(" [opcode = $%02x]\t", opcode);
				else
					dprintf("\t\t");
				break;
			case imm:
				dprintf(" #$%02x", fetch_operand(mode));
				break;
			case abs:
				dprintf(" $%x = #$%02x", fetch_parameter(mode), memory.peek(fetch_target(mode)) );
				break;
			case zp:
				dprintf(" $%x = #$%02x", fetch_parameter(mode), memory.peek(fetch_target(mode)));
				break;
			case ind_x:
				dprintf(" $%x, X = #$%02x", fetch_parameter(mode), memory.peek(fetch_target(mode)));
				break;
			case ind_y:
				dprintf(" $%x, Y = #$%02x", fetch_parameter(mode), memory.peek(fetch_target(mode)));
				break;
			case zpind_x:
				dprintf(" $%x, X = #$%02x", fetch_parameter(mode), memory.peek(fetch_target(mode)));
				break;
			case zpind_y:
				dprintf(" $%x, Y = #$%02x", fetch_parameter(mode), memory.peek(fetch_target(mode)));
				break;
			case indir:
				dprintf(" ($%x) = #$%02x", fetch_parameter(mode), memory.peek(fetch_target(mode)));
				break;
			case preindir:
				dprintf(" ($%x, X) = #$%02x", fetch_parameter(mode), memory.peek(fetch_target(mode)));
				break;
			case postindir:
				dprintf(" ($%x), Y = #$%02x", fetch_parameter(mode), memory.peek(fetch_target(mode)));
				break;
			case rel:
				dprintf(" $%x\t", fetch_operand(mode));
				break;
		}
		dprintf("\tA:%02x X:%02x Y:%02x SP:%02x F:%02x\n", a, x, y, sp, f);
	}

	int Cpu::execute_opcode(unsigned char opcode) {
		Mode mode = opcode_mode[opcode];
		//unsigned short data = fetch_operand(mode);
		int additional_cycles = opcode_cycles[opcode];
		
		//modified by fetch/instruction
		delta_cycles = 0;

		switch(opcode_instruction[opcode]) {
			case ADC: opADC(mode); break;
			case AND: opAND(mode); break;
			case ASL: opASL(mode); break;
			case BCC: opBCC(mode); break;
			case BCS: opBCS(mode); break;
			case BEQ: opBEQ(mode); break;
			case BIT: opBIT(mode); break;
			case BMI: opBMI(mode); break;
			case BNE: opBNE(mode); break;
			case BPL: opBPL(mode); break;
			case BRK: opBRK(mode); break;
			case BVC: opBVC(mode); break;
			case BVS: opBVS(mode); break;
			case CLC: opCLC(mode); break;
			case CLD: opCLD(mode); break;
			case CLI: opCLI(mode); break;
			case CLV: opCLV(mode); break;
			case CMP: opCMP(mode); break;
			case CPX: opCPX(mode); break;
			case CPY: opCPY(mode); break;
			case DEC: opDEC(mode); break;
			case DEX: opDEX(mode); break;
			case DEY: opDEY(mode); break;
			case EOR: opEOR(mode); break;
			case INC: opINC(mode); break;
			case INX: opINX(mode); break;
			case INY: opINY(mode); break;
			case JMP: opJMP(mode); break;
			case JSR: opJSR(mode); break;
			case LDA: opLDA(mode); break;
			case LDX: opLDX(mode); break;
			case LDY: opLDY(mode); break;
			case LSR: opLSR(mode); break;
			case NOP: opNOP(mode); break;
			case ORA: opORA(mode); break;
			case PHA: opPHA(mode); break;
			case PHP: opPHP(mode); break;
			case PLA: opPLA(mode); break;
			case PLP: opPLP(mode); break;
			case ROL: opROL(mode); break;
			case ROR: opROR(mode); break;
			case RTI: opRTI(mode); break;
			case RTS: opRTS(mode); break;
			case SBC: opSBC(mode); break;
			case SEC: opSEC(mode); break;
			case SED: opSED(mode); break;
			case SEI: opSEI(mode); break;
			case STA: opSTA(mode); break;
			case STX: opSTX(mode); break;
			case STY: opSTY(mode); break;
			case TAX: opTAX(mode); break;
			case TAY: opTAY(mode); break;
			case TSX: opTSX(mode); break;
			case TXA: opTXA(mode); break;
			case TXS: opTXS(mode); break;
			case TYA: opTYA(mode); break;
			default:
				//unknown opcode
				printf("Unknown opcode! Halting.\n");
				printf("PC:%04x A:%02x X:%02x Y:%02x\n", pc, a, x, y);
				::system("pause");
				exit(-1);
				break;
		}

		// increment PC if necessary (certain instructions do not)
		pc += opcode_operandsize[opcode];

		return additional_cycles + delta_cycles;
		//return additional_cycles;
	}

	inline void Cpu::branch(Mode mode) {
		auto new_pc = fetch_operand(mode);
		//1 Extra cycle for branch on same page, 2 extra cycles for branch on different page
		delta_cycles = ((new_pc ^ pc) & 0xff00) ? 2 : 1;
		//delta_cycles = 1;
		pc = new_pc;
	}

	inline void Cpu::opADC(Mode mode) {
		unsigned short data = fetch_operand(mode);
		unsigned short temp = data + a + ((f & FLAG_C) ? 1 : 0);
		set_zero(temp & 0xff);
		//Commented out because the 6502 on NES ignores this
		//if (f & FLAG_D) {
		//	if (((a & 0xf) + (data & 0xf) + ((f & FLAG_C) ? 1 : 0)) > 9)
		//		temp += 6;
		//	set_sign(temp);
		//	set_overflow(!((a ^ data) & 0x80) && ((a ^ temp) & 0x80));
		//	if (temp > 0x99) temp += 96;
		//	set_carry(temp > 0x99);
		//}
		//else {
			set_sign((unsigned char)temp);
			set_overflow(!((a ^ data) & 0x80) && ((a ^ temp) & 0x80));
			set_carry(temp > 0xff);
		//}
		a = (unsigned char)temp;
	}

	inline void Cpu::opAND(Mode mode) {
		a &= fetch_operand(mode);
		set_sign(a);
		set_zero(a);
	}

	inline void Cpu::opASL(Mode mode) {
		unsigned char data = (unsigned char)fetch_operand(mode);
		set_carry(data & 0x80);
		data <<= 1;
		set_sign(data);
		set_zero(data);
		if (mode == accum)
			a = data;
		else
			memory.write(fetch_target(mode), data);
		delta_cycles = 0;
	}

	inline void Cpu::opBCC(Mode mode) {
		if (!(f & FLAG_C))
			branch(mode);
		else
			++pc;
	}

	inline void Cpu::opBCS(Mode mode) {
		if (f & FLAG_C)
			branch(mode);
		else
			++pc;
	}

	inline void Cpu::opBEQ(Mode mode) {
		if (f & FLAG_Z)
			branch(mode);
		else
			++pc;
	}

	inline void Cpu::opBIT(Mode mode) {
		unsigned char data = (unsigned char)fetch_operand(mode);
		set_zero(a & data);
		set_sign(data);
		set_overflow(data & FLAG_V);
	}

	inline void Cpu::opBMI(Mode mode) {
		if (f & FLAG_N)
			branch(mode);
		else
			++pc;
	}

	inline void Cpu::opBNE(Mode mode) {
		if (!(f & FLAG_Z))
			branch(mode);
		else
			++pc;
	}

	inline void Cpu::opBPL(Mode mode) {
		if (!(f & FLAG_N))
			branch(mode);
		else
			++pc;
	}

	inline void Cpu::opBRK(Mode mode) {
		++pc;
		push(pc >> 8);
		push(pc & 0xff);
		set_break(true);
		push(f | 0x30);
		set_interrupt(true);
		pc = memory.read16(0xfffe);
	}

	inline void Cpu::opBVC(Mode mode) {
		if (!(f & FLAG_V))
			branch(mode);
		else
			++pc;
	}

	inline void Cpu::opBVS(Mode mode) {
		if (f & FLAG_V)
			branch(mode);
		else
			++pc;
	}

	inline void Cpu::opCLC(Mode mode) {
		set_carry(0);
	}

	inline void Cpu::opCLD(Mode mode) {
		set_decimal(0);
	}

	inline void Cpu::opCLI(Mode mode) {
		set_interrupt(0);
	}

	inline void Cpu::opCLV(Mode mode) {
		set_overflow(0);
	}

	inline void Cpu::opCMP(Mode mode) {
		unsigned short result = a - fetch_operand(mode);
		set_carry(result < 0x100);
		set_sign((unsigned char)result);
		set_zero(result & 0xff);
	}

	inline void Cpu::opCPX(Mode mode) {
		unsigned short result = x - fetch_operand(mode);
		set_carry(result < 0x100);
		set_sign((unsigned char)result);
		set_zero(result & 0xff);
	}

	inline void Cpu::opCPY(Mode mode) {
		unsigned short result = y - fetch_operand(mode);
		set_carry(result < 0x100);
		set_sign((unsigned char)result);
		set_zero(result & 0xff);
	}

	inline void Cpu::opDEC(Mode mode) {
		unsigned char data = (fetch_operand(mode) - 1) & 0xff;
		set_sign(data);
		set_zero(data);
		memory.write(fetch_target(mode), data);
		delta_cycles = 0;
	}

	inline void Cpu::opDEX(Mode mode) {
		--x;
		set_sign(x);
		set_zero(x);
	}

	inline void Cpu::opDEY(Mode mode) {
		--y;
		set_sign(y);
		set_zero(y);
	}

	inline void Cpu::opEOR(Mode mode) {
		a ^= fetch_operand(mode);
		set_sign(a);
		set_zero(a);
	}

	inline void Cpu::opINC(Mode mode) {
		unsigned char data = (fetch_operand(mode) + 1) & 0xff;
		set_sign(data);
		set_zero(data);
		memory.write(fetch_target(mode), data);
		delta_cycles = 0;
	}

	inline void Cpu::opINX(Mode mode) {
		++x;
		set_sign(x);
		set_zero(x);
	}

	inline void Cpu::opINY(Mode mode) {
		++y;
		set_sign(y);
		set_zero(y);
	}

	inline void Cpu::opJMP(Mode mode) {
		pc = fetch_target(mode);
	}

	inline void Cpu::opJSR(Mode mode) {
		unsigned short dest = pc + 1;
		push((dest >> 8) & 0xff);
		push(dest & 0xff);
		//TODO: perhaps this requires its own addressing mode?
		pc = fetch_target(mode);
	}

	inline void Cpu::opLDA(Mode mode) {
		a = (unsigned char)fetch_operand(mode);
		set_sign(a);
		set_zero(a);
	}

	inline void Cpu::opLDX(Mode mode) {
		x = (unsigned char)fetch_operand(mode);
		set_sign(x);
		set_zero(x);
	}

	inline void Cpu::opLDY(Mode mode) {
		y = (unsigned char)fetch_operand(mode);
		set_sign(y);
		set_zero(y);
	}

	inline void Cpu::opLSR(Mode mode) {
		unsigned char data = (unsigned char)fetch_operand(mode);
		set_carry(data & 0x01);
		data >>= 1;
		set_sign(data);
		set_zero(data);
		if (mode == accum)
			a = data;
		else
			memory.write(fetch_target(mode), data);
		delta_cycles = 0;
	}

	inline void Cpu::opNOP(Mode mode) {
	}

	inline void Cpu::opORA(Mode mode) {
		a |= fetch_operand(mode);
		set_sign(a);
		set_zero(a);
	}

	inline void Cpu::opPHA(Mode mode) {
		push(a);
	}

	inline void Cpu::opPHP(Mode mode) {
		push(f | 0x30);
	}

	inline void Cpu::opPLA(Mode mode) {
		a = pop();
		set_sign(a);
		set_zero(a);
	}

	inline void Cpu::opPLP(Mode mode) {
		f = pop() & ~0x30;
	}

	inline void Cpu::opROL(Mode mode) {
		unsigned short data = fetch_operand(mode);
		data <<= 1;
		if (f & FLAG_C)
			data |= 0x1;
		set_carry((data & 0x100) == 0x100);
		set_sign((unsigned char)data);
		set_zero((unsigned char)data);
		if (mode == accum)
			a = (unsigned char)data;
		else
			memory.write(fetch_target(mode), (unsigned char)data);
		delta_cycles = 0;
	}

	inline void Cpu::opROR(Mode mode) {
		unsigned short data = fetch_operand(mode);
		if (f & FLAG_C)
			data |= 0x100;
		set_carry(data & 0x1);
		data >>= 1;
		set_sign((unsigned char)data);
		set_zero((unsigned char)data);
		if (mode == accum)
			a = (unsigned char)data;
		else
			memory.write(fetch_target(mode), (unsigned char)data);
		delta_cycles = 0;
	}

	inline void Cpu::opRTI(Mode mode) {
		f = pop();
		unsigned short temp = pop();
		temp += (pop() << 8);
		pc = temp;
	}

	inline void Cpu::opRTS(Mode mode) {
		unsigned short temp = pop();
		temp += (pop() << 8) + 1;
		pc = temp;
	}

	inline void Cpu::opSBC(Mode mode) {
		unsigned short data = fetch_operand(mode);
		unsigned short temp = a - data - ((f & FLAG_C) ? 0 : 1);
		set_sign(temp);
		set_zero(temp & 0xff);
		set_overflow(((a ^ temp) & 0x80) && ((a ^ data) & 0x80));
		//Commented out because the 6502 on NES ignores this
		//if (f & FLAG_D) {
		//	if (((a & 0xf) - ((f & FLAG_C) ? 0 : 1)) < (data & 0xf))
		//		temp -= 6;
		//	if (temp > 0x99)
		//		temp -= 0x60;
		//}
		set_carry(temp < 0x100);
		a = (unsigned char)temp;
	}

	inline void Cpu::opSEC(Mode mode) {
		set_carry(1);
	}

	inline void Cpu::opSED(Mode mode) {
		set_decimal(1);
	}

	inline void Cpu::opSEI(Mode mode) {
		set_interrupt(1);
	}

	inline void Cpu::opSTA(Mode mode) {
		memory.write(fetch_target(mode), a);
		//no need since delta_cycles are added in fetch_operand not fetch_target
		//delta_cycles = 0;
	}

	inline void Cpu::opSTX(Mode mode) {
		memory.write(fetch_target(mode), x);
	}

	inline void Cpu::opSTY(Mode mode) {
		memory.write(fetch_target(mode), y);
	}

	inline void Cpu::opTAX(Mode mode) {
		set_sign(a);
		set_zero(a);
		x = a;
	}

	inline void Cpu::opTAY(Mode mode) {
		set_sign(a);
		set_zero(a);
		y = a;
	}

	inline void Cpu::opTSX(Mode mode) {
		set_sign(sp);
		set_zero(sp);
		x = sp;	
	}

	inline void Cpu::opTXA(Mode mode) {
		set_sign(x);
		set_zero(x);
		a = x;
	}

	inline void Cpu::opTXS(Mode mode) {
		sp = x;
	}

	inline void Cpu::opTYA(Mode mode) {
		set_sign(y);
		set_zero(y);
		a = y;
	}

	//============
	//System::
	System::System() : fractional_cycle(0), ppu(*this), apu(*this), cpu(*this) {
		
	}

	//TODO: Revert back to original solution when you have time.
	//i.e. use reference back pointers in the constructors, getting
	//rid of this init() junk and speeding up the program through
	//one less pointer dereference!
	void System::init() {
		cpu.init();
		ppu.init();
		apu.init();
		controller[0].init();
		controller[1].init();
	}

	void System::reset() {
		if (cartridge.loaded)
			cartridge.mapper->reset();
		cpu.reset();
		apu.init();
		ppu.init();
		controller[0].init();
		controller[1].init();
	}

	void System::set_render_target(uint32_t *target) {
		nametable_buffer = target;
	}

	//runs system for a single scanline
	int System::run_scanline(int extra_cycles) {
		extra_cycles = cpu.execute(113 - extra_cycles + (!fractional_cycle ? 1 : 0));
		fractional_cycle = (fractional_cycle + 1)%3;
		return extra_cycles;
	}

	//run system for an entire frame TODO: odd/even timing
	int System::run_frame(int extra_cycles) {
		int current_line = 0;
		apu.new_frame();
		if ((ppu.mask.enable_background || ppu.mask.enable_sprite) && cartridge.mapper->hsync(-1))
			cpu.irq();

		ppu.start_frame();

		ppu.status.vblank_started = false;
		extra_cycles = run_scanline(extra_cycles);
		extra_cycles += apu.run_scanline(current_line++);
		ppu.status.sprite0_hit = false;
		ppu.sprite0_hit_raised = false;

		if (nametable_buffer)
			ppu.render_name_table(nametable_buffer);

		for (int i=0; i<240; i++) {
			//Check if mapper would require IRQ (mainly for MMC3 type mapper)
			//Mapper 3 usually gets the line change on the 261th "dot". That means ~87 cpu cycles in.
			//extra_cycles = run_scanline(extra_cycles + (113 - 87));
			ppu.render_line(i);
			ppu.next_scanline();
			extra_cycles = cpu.execute(87 - extra_cycles);
			if ((ppu.mask.enable_background || ppu.mask.enable_sprite) && cartridge.mapper->hsync(i))
				extra_cycles += cpu.irq();
			extra_cycles = run_scanline(extra_cycles + 87);
			extra_cycles += apu.run_scanline(current_line++);
		}

		extra_cycles = run_scanline(extra_cycles);
		extra_cycles += apu.run_scanline(current_line++);

		extra_cycles += cpu.execute(5);
		if (ppu.ctrl.enable_nmi)
			extra_cycles += cpu.nmi();
		ppu.vblank();
		extra_cycles += 5;
		
		for (int i=0; i<20; i++) {
			extra_cycles = run_scanline(extra_cycles);
			extra_cycles += apu.run_scanline(current_line++);
		}
		ppu.vblank_end();
		apu.end_frame();

		return extra_cycles;
	}
	
}
